﻿<%@ Page language="C#" MasterPageFile="~masterurl/default.master" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ContentPlaceHolderId="PlaceHolderAdditionalPageHead" runat="server">
    <SharePoint:ScriptLink name="sp.js" runat="server" OnDemand="true" LoadAfterUI="true" Localizable="false" />
</asp:Content>

<asp:Content ContentPlaceHolderId="PlaceHolderMain" runat="server">
    <WebPartPages:WebPartZone runat="server" FrameType="TitleBarOnly" ID="full" Title="loc:full" />
    <div>
        <h1>Trial Expired</h1>
    </div>
    <div>
        <p>How time flies! Your trial period has just expired, so let's see what we can do next?</p>
        <p><strong>If you liked our calendar</strong>, why not purchase the full license at the <a href="https://store.office.com/en-us/app.aspx?assetid=WA104380414">Microsoft Store</a>?</p>
        <p><strong>If there was a problem</strong> with this app, please drop us a note to <a href="mailto:celgo@celgo.net">celgo@celgo.net</a> so that we can make this calendar suit your needs.</p>
        <p>Thanks for trying it out - you're the best!</p>
        <p>Love,<br />Celgo</p>
        <img src="../Images/Celgo.png" width="96" height="96" />
    </div>
</asp:Content>
