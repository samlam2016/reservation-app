﻿<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" Language="C#" %>
 
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<WebPartPages:AllowFraming ID="AllowFraming" runat="server" />

<SharePoint:ScriptLink name="MicrosoftAjax.js" runat="server" LoadAfterUI="true" Localizable="false"/>


<SharePoint:ScriptLink name="clienttemplates.js" runat="server" LoadAfterUI="true" Localizable="false"/>
<SharePoint:ScriptLink name="clientforms.js" runat="server" LoadAfterUI="true" Localizable="false"/>
<SharePoint:ScriptLink name="clientpeoplepicker.js" runat="server" LoadAfterUI="true" Localizable="false"/>
<SharePoint:ScriptLink name="autofill.js" runat="server" LoadAfterUI="true" Localizable="false"/>

<SharePoint:ScriptLink name="sp.js" runat="server" LoadAfterUI="true" Localizable="false"/>
<SharePoint:ScriptLink name="sp.core.js" runat="server" LoadAfterUI="true" Localizable="false"/>
<SharePoint:ScriptLink name="sp.runtime.js" runat="server" LoadAfterUI="true" Localizable="false"/>
 
<!-- The following tells SharePoint to allow this page to be hosted in an IFrame -->
<WebPartPages:AllowFraming runat="server" />
 
<html>
<head>
    <title>Calendar</title>
    <!-- scripts at the end -->
    <!-- sharepoint -->
    <!-- <script type="text/javascript" src="/_layouts/15/sp.runtime.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.js"></script> -->


    <!-- <SharePoint:ScriptLink Language="javascript" Name="/_layouts/15/sp.runtime.js" runat="server" OnDemand="true"></SharePoint:ScriptLink>
    <SharePoint:ScriptLink Language="javascript" Name="/_layouts/15/sp.js" runat="server" OnDemand="true"></SharePoint:ScriptLink> -->
    <!-- <script type="text/javascript" src="/_layouts/15/MicrosoftAjax.js"></script> -->

    <!--
    <script type="text/javascript" src="/_layouts/15/MicrosoftAjax.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.runtime.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.core.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.js"></script>

    <script type="text/javascript" src="/_layouts/15/clienttemplates.js"></script>
    <script type="text/javascript" src="/_layouts/15/clientforms.js"></script>
    <script type="text/javascript" src="/_layouts/15/clientpeoplepicker.js"></script>
    <script type="text/javascript" src="/_layouts/15/autofill.js"></script>
    -->


    <!-- our app -->
    <script src="../Scripts/custom/fullcalendar/moment.min.js"></script>
    <script src="../Scripts/custom/jquery/jquery.min.js"></script>


    <script src="../Scripts/custom/jquery/jquery.datetimepicker.full.min.js"></script>
    <script src="../Scripts/custom/jquery/xml2json.js"></script>
    <script src="../Scripts/custom/fullcalendar/fullcalendar-2.7.3.js"></script>
    <script src="../Scripts/custom/fullcalendar/scheduler-1.3.2.js"></script>
    <script src="../Scripts/custom/jquery/jquery.qtip.min.js"></script>
    <script src="../Scripts/custom/auth.js"></script>
    <script src="../Scripts/custom/type-specific.js"></script>
    <script src="../Scripts/custom/filtering.js"></script>
    <script src="../Scripts/custom/main.js"></script>
    <script src="../Scripts/custom/jquery/jquery.editable-select.min.js"></script>
    <script src="../Scripts/custom/jquery/jquery.cookie.js"></script>

    
    <link href='../Content/Styles/fullcalendar-2.7.3.css' rel='stylesheet' />
    <link href='../Content/Styles/fullcalendar-2.7.3.print.css' rel='stylesheet' media='print' />
    <link href='../Content/Styles/scheduler-1.3.2.css' rel='stylesheet' />
    <link href='../Content/Styles/main.css' rel='stylesheet' />
    <link href='../Content/Styles/jquery.qtip.min.css' rel='stylesheet' />
    <link href='../Content/Styles/jquery.datetimepicker.min.css' rel='stylesheet' />
    <link href='../Content/Styles/jquery.editable-select.min.css' rel='stylesheet' />
    <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css' rel="Stylesheet" />

</head>
<body >

    <!-- calendar will be placed here -->
    <div id="c"></div>

    <!-- popup windows -->
    <div id="createScreen" class="modalPopup">
        <div>
            <div>
                <div>
                    <h2 class="fcPopupHeader">Create new event</h2>
                    <div class="scheduleFormInputRow">
                        <label>Subject</label>
                        <input name="location" class="formLocation" />
                        <!--
                        <select name="location" class="formLocation">
                            <option value=""></option>
                        </select>
                        -->
                    </div>

                    <!--
                    <div>
                        <label class="cc-required">Resource</label>
                        <select name="resourceSelect" class="resourceSelect">
                            <option value=""></option>
                        </select>
                    </div>
                    -->

                    <div class="scheduleFormInputRow">
                        <label class="cc-required">Start Date</label>
                        <input name="startDate" class="formStartDate">
                        <input type="checkbox" id="fullDayCheckbox-create" class="fullDayCheckbox">
                        <label for="fullDayCheckbox-create">Full day</label>
                    </div>

                    <div class="scheduleFormInputRow">
                        <label class="cc-required">End Date</label>
                        <input name="endDate" class="formEndDate">
                    </div>

                    <div class="scheduleFormInputRow">
                        <label>Type / Color<div class="cc-edit-form-color-sampler"></div></label>
                        <select name="travelType" class="formTravelType">
                            <option value=""></option>
                        </select>
                    </div>

                    <div class="popupButtons">
                    	<div class="formErrorMessage"></div>
                        <span class="cssSpinner"></span>
                        <button type="button" class="cc-button-cancel">Cancel</button>
                        <button type="button" class="cc-button-ok">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div id="filterScreen" class="modalPopup">
        <div>
            <div>
                <div>
                    <h2 class="fcPopupHeader">Filter by Grade</h2>
                    <div class="hasBorderBottom">
                        <input type="checkbox" id="toggleAll">
                        <label for="toggleAll">Select All</label>
                    </div>
                    
                    <div id="filterCheckboxesContainer">
                    </div>                    
                    
                    <div class="popupButtons">
                        <span class="cssSpinner"></span>
                        <button type="button">Filter</button>
                        <button type="button">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<div id="editScreen" class="modalPopup">
        <div>
            <div>
                <div>
                    <h2 class="fcPopupHeader">Edit event</h2>
                    <input type="hidden" name="spEventId" id="spEventId">
                    <div class="scheduleFormInputRow">
                        <label>Subject</label>
                        <input name="location" class="formLocation" />
                        <!--
                        <select name="location" class="formLocation">
                            <option value=""></option>
                        </select>
                        -->
                    </div>

                    <!--
                    <div>
                        <label class="cc-required">Resource</label>
                        <select name="resourceSelect" class="resourceSelect">
                            <option value=""></option>
                        </select>
                    </div>
                    -->

                    <div class="scheduleFormInputRow">
                        <label class="cc-required">Start Date</label>
                        <input name="startDate" class="formStartDate">
                        <input type="checkbox" id="fullDayCheckbox-edit" class="fullDayCheckbox">
                        <label for="fullDayCheckbox-edit">Full day</label>
                    </div>

                    <div class="scheduleFormInputRow">
                        <label class="cc-required">End Date</label>
                        <input name="endDate" class="formEndDate">
                    </div>

                    <div class="scheduleFormInputRow">
                        <label>Type / Color<div class="cc-edit-form-color-sampler"></div></label>
                        <select name="travelType" class="formTravelType">
                            <option value=""></option>
                        </select>
                    </div>

                    <div class="popupButtons">
                    	<div class="formErrorMessage"></div>
                        <span class="cssSpinner"></span>
                        <button type="button" class="cc-button-cancel">Cancel</button>
                        <button type="button" class="cc-button-ok">Save</button>
                    </div>
                    <div class="deleteEvent">
                    	<span class="deleteEventLink">Delete</span>
                    </div>
                    <div class="reallyDelete">
                    	<p class="reallyDeleteEvent">Really delete?</p>
                    	<span class="yesDeleteEvent">Yes</span>
                    	<span class="noDeleteEvent">No</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="firstRunScreen" class="modalPopup">
        <div>
            <div>
                <div>
                    <h2 class="fcPopupHeader">Welcome</h2>
                    <h2>This calendar add-in comes with three layout options. Choose one that suits your needs!</h2>
                    <div class="calendarTypeOption">
                        <p><input type="radio" name="calendarType" value="meetings" /> Simple Schedule</p>
                        <div></div>
                        <span class="calendarTypeDescription">Keep track of everyone's meetings within your company with a daily, weekly or monthly view.</span>
                    </div>
                    <div class="calendarTypeOption">
                        <p><input type="radio" name="calendarType" value="roombooking" /> Room Booking / Team Calendar</p>
                        <div></div>
                        <span class="calendarTypeDescription">Book rooms in your offices or other venues, with a view grouped by floors or buildings.</span>
                    </div>
                    <div class="calendarTypeOption">
                        <p><!-- <input type="radio" name="calendarType" value="carbooking" disabled />--> Vehicle Booking - Coming Soon!</p>
                        <div></div>
                        <span class="calendarTypeDescription">Reserve company vehicles and drivers, and keep track of their availability.</span>
                    </div>
                    <p class="buttonRow-right">
                        <button type="button" class="cc-button-ok" disabled="disabled">Finish Setup</button>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div id="bookRoomScreen" class="modalPopup">
        <div>
            <div>
                <div>
                    <h2 class="fcPopupHeader">Booking Form</h2>
                    <div class="modalColsContainer">
                        <div class="modalCol1">
                            <input type="hidden" class="hiddenEventId" value="" />
                            <div>
                                 <label class="cc-required">Starts at</label>
                                 <input name="startDate" class="formStartDate">
                             </div>
                             <div>
                                 <label class="cc-required">Ends at</label>
                                 <input name="endDate" class="formEndDate">
                             </div>
                            <div>
                                <label>Type<div class="cc-edit-form-color-sampler"></div></label>
                                <select class="formTravelType">
                                    <option value="">Select color</option>
                                </select>
                            </div>
                            <div>
                                <label>Title</label>
                                <input class="formSubject" />
                            </div>
                            <div>
                                <label class="cc-valign-top">Notes/Requirements</label>
                                <textarea class="notesRequirements"></textarea>
                            </div>
                            <div>
                                <label>Booked by</label>
                                <input class="currentUserName" disabled="disabled"/>
                                <span class="cc-edit-link">Edit</span>
                            </div>
                        </div>
                        <div class="modalCol2">
                            <div class="resourceSelect">
                                <span class="cc-required">
                                    <select class="resourceSelect">
                                        <option value="">Select a room</option>
                                    </select>
                                </span>
                            </div>
                            <div>
                                <p class="resourceDescription"></p>
                            </div>
                            <div class="cc-attendees-picker">
                                <label class="cc-valign-top">Attendees</label>
                                <div id="PeopleEditor1"></div>
                                <!-- 

                                -->
                                <!-- <input class="formAttendees"/> -->
                            </div>
                        </div>
                        <div class="buttonRow-left">
                            <button type="button" class="cc-button-danger">Delete</button>
                        </div>
                        <div class="buttonRow-right">
                            <span class="cssSpinner"></span>
                            <button type="button" class="cc-button-cancel">Cancel</button>
                            <button type="button" class="cc-button-ok">Book</button>
                            <div class="formErrorMessage">test</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="bookCarScreen" class="modalPopup">
        <div>
            <div>
                <div>
                    <h2 class="fcPopupHeader">Book a Vehicle</h2>
                    <div class="modalColsContainer">
                        <div class="modalCol1">
                            <input type="hidden" class="hiddenEventId" value="" />
                            <div>
                                 <label class="cc-required">Starts at</label>
                                 <input name="startDate" class="formStartDate">
                             </div>
                             <div>
                                 <label class="cc-required">Ends at</label>
                                 <input name="endDate" class="formEndDate">
                             </div>
                            <div>
                                <label>Type</label>
                                <select class="formTravelType">
                                    <option value="">Select color</option>
                                </select>
                            </div>
                            <div>
                                <label>Title</label>
                                <input class="formSubject" />
                            </div>
                            <div>
                                <label class="cc-valign-top">Notes/Requirements</label>
                                <textarea class="notesRequirements"></textarea>
                            </div>
                            <div>
                                <label>Booked by</label>
                                <input class="currentUserName" disabled="disabled"/>
                                <span class="cc-edit-link">Edit</span>
                            </div>
                        </div>
                        <div class="modalCol2">
                            <div class="resourceSelect">
                                <span class="cc-required">
                                    <select class="resourceSelect">
                                        <option value="">Select a vehicle</option>
                                    </select>
                                </span>
                            </div>
                            <div>
                                <p class="resourceDescription"></p>
                            </div>
                            <div class="handledbySelect">
                                <span class="cc-required">
                                    <select class="handledbySelect">
                                        <option value="">Select a driver</option>
                                        <option value="1">Mr. Wong</option>
                                        <option value="2">Mr. Lee</option>
                                    </select>
                                </span>
                            </div>
                            <div>
                                <p class="handledbyDescription"></p>
                            </div>
                            <div class="cc-attendees-picker">
                                <label class="cc-valign-top">Attendees</label>

                                <!-- <input class="formAttendees"/> -->
                            </div>
                        </div>
                        <div class="buttonRow-left">
                            <button type="button" class="cc-button-danger">Delete</button>
                        </div>
                        <div class="buttonRow-right">
                            <span class="cssSpinner"></span>
                            <button type="button" class="cc-button-cancel">Cancel</button>
                            <button type="button" class="cc-button-ok">Book</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="cc-settings-screen" class="modalPopup">
        <div>
            <div>
                <div>
                    <h2 class="fcPopupHeader">Calendar Settings</h2>
                    <div class="cc-tab-bar">
                        <button type="button" class="cc-tab-active" data-tab-number="1">Types/Colors</button><button type="button" data-tab-number="2">Resources</button><button type="button" data-tab-number="3">Other</button><button type="button" data-tab-number="4">About</button>
                        <img src="../Images/AppIcon.png" class="cc-small-app-icon" />
                    </div>
                    <div class="cc-tabs">
                        <div class="cc-tab">
                            <p class="cc-tab-description-text">You can add, edit or remove event types/colors here. Click any field to edit it.</p>
                            <div class="cc-settings-list-header">
                                <span>Type</span>
                                <span>Color code</span>
                            </div>
                            <div class="cc-types-list">

                            </div>
                            <div class="cc-add-row cc-add-row-colors">
                                <button type="button"><i class="fa fa-plus" aria-hidden="true"></i> Add</button>
                            </div>
                        </div>
                        <div class="cc-tab">
                            <p class="cc-tab-description-text">You can add, edit or remove resources here. Click any field to edit it.</p>
                            <div class="cc-settings-list-header">
                                <span>Group</span>
                                <span>Name</span>
                                <span>Order</span>
                            </div>
                            <div class="cc-resources-list">

                            </div>
                            <div class="cc-add-row cc-add-row-resources">
                                <button type="button"><i class="fa fa-plus" aria-hidden="true"></i> Add</button>
                            </div>
                        </div>
                        <div class="cc-tab">
                            <p class="cc-tab-description-text">Other settings will be here, such as configurable button/column labels.</p>
                        </div>
                        <div class="cc-tab">
                            <p class="cc-align-center"><div class="celgo-logo"></div></p>
                            <p class="cc-tab-description-text"><strong>Developed by <a href="http://www.celgo.net">Celgo</a></strong> and licensed under GPLv3 license. <span id="cc-more-link">More...</span></p>
                            <div id="gplv3details" class="cc-align-left">
                                <p><strong>Copyright 2016 Celgo.</strong></p>
                                <p>This program is free software: you can redistribute it and/or modify
                                    it under the terms of the GNU General Public License as published by
                                    the Free Software Foundation, either version 3 of the License, or
                                    (at your option) any later version.</p>

                                    <p>This program is distributed in the hope that it will be useful,
                                    but WITHOUT ANY WARRANTY; without even the implied warranty of
                                    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
                                    GNU General Public License for more details.</p>

                                    <p>You should have received <a href="http://www.gnu.org/licenses/gpl-3.0.en.html">a copy of the GNU General Public License</a>
                                    along with this program. If not, see http://www.gnu.org/licenses/.</p>

                                    <p>DISCLAIMER OF WARRANTY. THE APPLICATION IS LICENSED "AS-IS," "WITH ALL FAULTS," AND "AS AVAILABLE." YOU BEAR ALL RISK OF USING IT. THE APPLICATION PROVIDER, ON BEHALF OF ITSELF, MICROSOFT, WIRELESS CARRIERS OVER WHOSE NETWORK THE APPLICATION IS DISTRIBUTED, AND ITS AND THEIR RESPECTIVE AFFILIATES, VENDORS, AGENTS, AND SUPPLIERS (“DISTRIBUTORS”), GIVES NO EXPRESS WARRANTIES, GUARANTEES, OR CONDITIONS UNDER OR IN RELATION TO THE APPLICATION. YOU MAY HAVE ADDITIONAL CONSUMER RIGHTS UNDER YOUR LOCAL LAWS WHICH THIS AGREEMENT CANNOT CHANGE. TO THE EXTENT PERMITTED UNDER YOUR LOCAL LAWS, DISTRIBUTORS EXCLUDE ANY IMPLIED WARRANTIES OR CONDITIONS, INCLUDING THOSE OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.</p>
                                    
                                    <p class="cc-align-center">***</p>

                                    <p>LIMITATION ON AND EXCLUSION OF REMEDIES AND DAMAGES. TO THE EXTENT NOT PROHIBITED BY LAW, YOU CAN RECOVER FROM THE APPLICATION PROVIDER ONLY DIRECT DAMAGES UP TO THE AMOUNT YOU PAID FOR THE APPLICATION. YOU AGREE NOT TO SEEK TO RECOVER ANY OTHER DAMAGES, INCLUDING CONSEQUENTIAL, LOST PROFITS, SPECIAL, INDIRECT OR INCIDENTAL DAMAGES FROM ANY DISTRIBUTOR.</p>

                                    <p>This limitation applies to anything related to the Application, services made available through the Application, or content (including code) on third party Internet sites;
                                    and claims for breach of contract; breach of warranty, guarantee or condition; or strict liability, negligence, or other tort to the extent permitted by applicable law.</p>

                                    <p>It also applies even if repair, replacement, or a refund for the Application does not fully compensate you for any losses; or Distributor knew or should have known about the possibility of the damages.</p>

                                    <p><strong>Microsoft does not provide any support services for this software.</strong></p>

                                    <p class="cc-align-center">***</p>

                                    <p>To obtain a copy of the source code of this program, please write to:</p>

                                    <blockquote>
                                        Development Team<br />
                                        Celgo Systems Limited<br />
                                        6/F 109-115 Queens Road East<br />
                                        Hong Kong
                                    </blockquote>
                            </div>
                            <div class="cc-inline-block">
                                <p class="cc-align-left">This Sharepoint Add-In uses the following open-source software:</p>
                                <ul>
                                    <li><a href="http://fullcalendar.io/">FullCalendar</a> by Adam Shaw. Licensed under the <a href="https://github.com/fullcalendar/fullcalendar/blob/master/LICENSE.txt">MIT license</a>.</li>
                                    <li><a href="http://fullcalendar.io/scheduler/">Scheduler</a> by Adam Shaw. Licensed under the <a href="http://www.gnu.org/licenses/gpl-3.0.en.html">GPLv3 license</a>.</li>
                                    <li><a href="https://jquery.org/">jQuery</a> by jQuery Foundation. Licensed under the <a href="https://jquery.org/license/">MIT License</a>.</li>
                                    <li><a href="http://qtip2.com/">qTip2</a> by Craig Michael Thompson. Licensed under the <a href="https://github.com/qTip2/qTip2/blob/master/LICENSE">MIT license</a>.</li>
                                    <li><a href="https://github.com/indrimuska/jquery-editable-select">jQuery Editable Select</a> by Indri Muska. Licensed under the <a href="https://github.com/indrimuska/jquery-editable-select">MIT license</a>.</li>
                                    <li><a href="http://xdsoft.net/jqplugins/datetimepicker/">Datetimepicker</a> by http://xdsoft.net. Licensed under the <a href="https://github.com/xdan/datetimepicker/blob/master/MIT-LICENSE.txt">MIT license</a>.</li>
                                    <li><a href="http://projects.lukehaas.me/css-loaders/">Single Element CSS Spinners</a> by Luke Haas. Licensed under the <a href="https://github.com/lukehaas/css-loaders/blob/step2/LICENSE">MIT license</a>.</li>
                                    <li><a href="https://github.com/sparkbuzz/jQuery-xml2json">jQuery XML2JSON</a> by Josef van Niekerk. Licensed under the <a href="https://github.com/sparkbuzz/jQuery-xml2json">MIT license</a>.</li>
                                    <li><a href="https://github.com/carhartl/jquery-cookie">jQuery Cookie</a> by Klaus Hartl. Licensed under the <a href="https://github.com/carhartl/jquery-cookie/blob/master/MIT-LICENSE.txt">MIT license</a>.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="buttonRow-right">
                            <span class="cssSpinner"></span>
                            <button type="button" class="cc-button-ok">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>