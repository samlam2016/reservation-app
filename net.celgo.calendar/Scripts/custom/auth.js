﻿/** checks whether the user logged in to sharepoint is authorized to create/edit events; store result in global var */
function verifyUserAuthorizedToEdit() {
    /*for testing*/
    globalUserAllowedToEdit = true;

    //show Create button if authorized to edit
    if (globalUserAllowedToEdit === true) {
        //disabled in favour of left-side New button
        //$(".fc-button-group:first-child button:first-of-type").css("display", "block");
    }

    /* for production:
    var currentContext = new SP.ClientContext.get_current();
    var currentWeb = currentContext.get_web();

    var currentUser = currentContext.get_web().get_currentUser();
    currentContext.load(currentUser);	
    
    var allGroups = currentWeb.get_siteGroups();
    currentContext.load(allGroups);

    var group = allGroups.getByName(globalAuthorizedUsersGroup);
    currentContext.load(group);
    
    var groupUsers = group.get_users();
    currentContext.load(groupUsers);

	currentContext.executeQueryAsync(OnSuccess,OnFailure);	

    function OnSuccess(sender, args) {
        var groupUserEnumerator = groupUsers.getEnumerator();
        while (groupUserEnumerator.moveNext()) {
            var groupUser = groupUserEnumerator.get_current();
            if (groupUser.get_id() == currentUser.get_id()) {
                globalUserAllowedToEdit = true;
                $(".fc-button-group:first-child button:first-of-type").show();
                break;
            }
        }
        if (globalUserAllowedToEdit == false) {
            hideEditingControls();  
        }
    }

    function OnFailure(sender, args) {
        globalUserAllowedToEdit = false;
        hideEditingControls();
    }
    */
}

/** hides all buttons and functions that allow user to create/edit events */
function hideEditingControls() {
    /* hide create button */
    //$(".fc-button-group:first-child button:first-of-type").fadeOut();

    /* hide cursor:pointer from events - user can't click to edit */
    //TODO needs to be done on event.render //$(".fc-event").css("cursor", "default");
}

/** checks whether user can edit event by clicking on it, and if so, then show the window */
function checkIfCanShowEditScreen(event) {
    if (globalUserAllowedToEdit === true) {
        showEditScreen(event);
    }
}

/** checks whether user can create events by clicking on create button; if yes, show the window */
function checkIfCanShowCreateScreen(date) {
    if (globalUserAllowedToEdit === true) {
        showNewScreenDependingOnType(date);
    }
}

/** async query SP for current user's name and output it into target jQuery selector; selector must be an input */
function outputCurrentUserName(targetSelector) {
    var context = SP.ClientContext.get_current();
    var user = context.get_web().get_currentUser();

    context.load(user);
    context.executeQueryAsync(
        function () {
            $(targetSelector).val(user.get_title());
        }, 
        function() {
            $(targetSelector).val("Error getting your name");
        });
}