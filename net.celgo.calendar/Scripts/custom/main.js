﻿/*
 * Calendar by Celgo
 *
 * 
 * Configuration below:
 */

/** Sharepoint URL */
var globalSharepointUrl = "";//_spPageContextInfo.webAbsoluteUrl;

/** name of the Sharepoint group that contains users authorized to edit the calendar */
//var globalAuthorizedUsersGroup = "";

/** license key for the scheduler plugin: can be commercial, CC or GPL */
var schedulerLicenseKey = 'GPL-My-Project-Is-Open-Source';

/*
 * End of configuration.
/*


/* Sharepoint list querying setup */
var context;
var web;
var user;
var spHostUrl = decodeURIComponent(getQueryStringParameter('SPHostUrl'));
var spAppWebUrl = decodeURIComponent(getQueryStringParameter('SPAppWebUrl'));
var parentContext;
globalSharepointUrl = spAppWebUrl;
var globalRequestDigest = $("#__REQUESTDIGEST").val();
var globalRequestDigestTimeout = 1800;
var isDraggingOnMobile = false;

function refreshRequestDigest() {
    //get request digest which is used for query authorizations - it is not available if running from iframe
    $.ajax({
        async: false,
        url: globalSharepointUrl + "/_api/contextinfo",
        type: "POST",
        headers: {
            "accept": "application/json; odata=verbose"
        },
        success: function (d) {
            globalRequestDigest = d.d.GetContextWebInformation.FormDigestValue;
            globalRequestDigestTimeout = (d.d.GetContextWebInformation.FormDigestTimeoutSeconds - 10) * 1000;
            setTimeout(refreshRequestDigest, globalRequestDigestTimeout);
        },
        error: function (d) {
            setTimeout(refreshRequestDigest, 10000); //if error, try refreshing digest every ten seconds, otherwise user won't be able to edit
            console.log("Error fetching request digest from server.");
            console.log(d);
        }
    });
}

refreshRequestDigest();

function getQueryStringParameter(urlParameterKey) {
    var params = document.URL.split('?')[1].split('&');
    var strParams = '';
    for (var i = 0; i < params.length; i++) {
        var singleParam = params[i].split('=');
        if (singleParam[0] === urlParameterKey)
            return decodeURIComponent(singleParam[1]);
    }
}

context = new SP.ClientContext.get_current();
parentContext = new SP.AppContextSite(context, spAppWebUrl);
web = parentContext.get_web();

/* end of sharepoint setup */

/* Otherwise annoying error messages in IE11 due to SP scripts that do not prevent normal execution */
window.onerror = function (message, source, lineno) {
    console.log("An error raised; please debug. Could be Sharepoint-related error.\n" + message + "\n" + source + ":" + lineno);
    return true;
};

/* resize handler for iframe when viewing as app part - set width to 100% */
window.Communica = window.Communica || {};

Communica.Part = {
    senderId: '',

    init: function () {
        this.senderId = decodeURIComponent(getQueryStringParameter("SenderId"));
        this.adjustSize();
    },

    adjustSize: function () {
        var step = 30,
            newHeight,
            contentHeight = ($('#c').height()) + 250,
            resizeMessage = '<message senderId={Sender_ID}>resize({Width}, {Height})</message>';

        //when adding new app part, the calendar is not rendered yet so can't determine its height
        if (contentHeight <= 760) {
            contentHeight = 760;
        }

        newHeight = (step - (contentHeight % step)) + contentHeight;

        resizeMessage = resizeMessage.replace("{Sender_ID}", this.senderId);
        resizeMessage = resizeMessage.replace("{Height}", newHeight);
        resizeMessage = resizeMessage.replace("{Width}", "100%");

        window.parent.postMessage(resizeMessage, "*");
    }
};

/* for iframe/app part, import Sharepoint's CSS */
//Build absolute path to the layouts root with the spHostUrl
var layoutsRoot = spHostUrl + '/_layouts/15/';

//Create a Link element for the defaultcss.ashx resource
var linkElement = document.createElement('link');
linkElement.setAttribute('rel', 'stylesheet');
linkElement.setAttribute('href', layoutsRoot + 'defaultcss.ashx');

//Add the linkElement as a child to the head section of the html
var headElement = document.getElementsByTagName('head');
headElement[0].appendChild(linkElement);


/* get the app's virtual instance ID */
var instanceId = decodeURIComponent(getQueryStringParameter('InstanceIdentifier'));


/* Celgo Calendar code begins */

/** keeps snapshots of data, this will be used for lookups or to restore them after filtering without extra query to db */
var originalResourcesSnapshot = [];
var globalTravelTypes = [];
var globalTravelMembers = [];
var globalLocations = [];
var globalUserAllowedToEdit = false; // insecure and easily circumvented; actual security should be done server side
var globalCalendarSettings = null;
var globalPopulateSettingsStatus = { resourcesLoaded: false, colorsLoaded: false, alreadyPopulated: false };

var C = $("#c");

/** on document.ready, first thing to do is to check whether we're running first time; show first run screen or show the calendar */
$(function () { // document ready
    C = $("#c"); //need to reinitialize if scripts moved to the top part of the page
    //generate the calendar
    if (isFirstRun()) {
        showFirstRunScreen();
    }
    else {
        createFullCalendar();
        doAfterCalendarLoadAttachments();
    }

    //handle autoresize to 100% width for iframe
    Communica.Part.init();

    //prevent incorrect touchend on mobile
    $("body").on("touchmove", function () {
        isDraggingOnMobile = true;
    });
    $("body").on("touchstart", function () {
        isDraggingOnMobile = false;
    });
    $(".fullDayCheckbox").change(function () {
        var thisCheckboxStatus = $(this).is(":checked");
        var form = $(this).parent().parent().children();
        var startDateInput = form.find(".formStartDate");
        var fullDayStartDate = moment(startDateInput.val());
        fullDayStartDate.set('hour', 0);
        fullDayStartDate.set('minute', 0);
        startDateInput.val(fullDayStartDate.format('YYYY-MM-DD HH:mm'));
        var fullDayEndDate = fullDayStartDate.add(24, 'hours');
        //var fullDayEndDate = moment(form.find(".formEndDate").val());
        form.find(".formEndDate").val(fullDayEndDate.format('YYYY-MM-DD HH:mm')).prop("disabled", thisCheckboxStatus);
    });
});

/** shows create new meeting screen for the Meetings calendar type */
function showCreateMeetingScreen(startDate) {
    // set startdate when clicked on column header, otherwise it was set during document.ready
    if (startDate) {
        startDate = moment(startDate);
        $("#createScreen .formStartDate").prop("value", startDate.format('YYYY-MM-DD HH:mm'));
        $("#createScreen .formEndDate").prop("value", startDate.add(1, 'days').format('YYYY-MM-DD HH:mm'));
    }
    
    $("#createScreen .formFullName").val("");
    $("#createScreen .formTravelType").val("");
    $("#createScreen .formLocation").val("");
    
    attachClickOutsideToClose("create");

    $("#createScreen").fadeIn();
}

/** populates names dropdowns - DEPRECATED */
function populateNamesDropdown() {

    /*
    var url = globalSharepointUrl + "_api/web/lists/getbytitle('_TravelMember')/items";

    $.ajax({
        url: url,
        type: 'GET',
        headers: {
            "accept": "application/json; odata=verbose"
        },
        success: function(d) {
        	// save data to global for converting id to full name
        	globalTravelMembers = d.d.results;
        	
        	// create select and convert it to a custom dropdown
            var outputHtml = '';
            for (var i = 0; i < d.d.results.length; i++) {
		        outputHtml +=
		            '<option value="' + d.d.results[i].Id + '">' +
		            d.d.results[i].Title +
		            '</option>';
    		};
            $(".formFullName").append(outputHtml);
            
            $("#createScreen .formFullName").editableSelect({effects: 'slide'});
            $("#editScreen .formFullName").editableSelect({effects: 'slide'});
            
        },
        error: function(d) {
        	console.log("Error getting names from server.");
        	console.log(d);
        	$("#createScreen .formFullName").editableSelect({effects: 'slide'});
        	$("#editScreen .formFullName").editableSelect({effects: 'slide'});
        	$(".formFullName").val("Error getting names from server. Refresh the page or contact administrator.");
    	}
    });
*/
}

/** populates dropdowns with travel types */
function populateTravelTypes() {
		var outputHtml = '<option value=""></option>';
		for (var i = 0; i < globalTravelTypes.length; i++) {
			outputHtml +=
				'<option value="' + globalTravelTypes[i].Id + '">' + //used to be ...[i].Title
				((globalTravelTypes[i].EventTypeName === null || globalTravelTypes[i].EventTypeName === "null") ? "(no name)" : globalTravelTypes[i].EventTypeName) +
				'</option>';
		}
		$(".formTravelType").html(outputHtml);		
}

/** DEPRECATED */
function populateLocationsDropdown() {
    //$("#createScreen .formLocation").editableSelect();
    //$("#editScreen .formLocation").editableSelect();

    /*
    var url = globalSharepointUrl + "_api/web/lists/getbytitle('_Location')/items";
	
	$.ajax({
		url: url,
		type: 'GET',
		headers: {
			"accept": "application/json; odata=verbose"
		},
		success: function(d) {
			var dd = d.d.results;
			globalLocations = dd;
			var outputHtml = '';
			for (var i = 0; i < dd.length; i++) {
				outputHtml += 
					'<option value="' + dd[i].Id + '">' +
					dd[i].Title +
					'</option>';
			}
			$(".formLocation").append(outputHtml);
			$("#createScreen .formLocation").editableSelect();
			$("#editScreen .formLocation").editableSelect();
		},
		error: function(d) {
			console.log("Error getting locations from server. ");
			console.log(d);
			$(".formLocation").append("<option>Error getting locations from server. Refresh the page or contact administrator.</option>");
			$("#createScreen .formLocation").editableSelect();
			$("#editScreen .formLocation").editableSelect();
		}
	});
*/
}

/** saves new event for Meeting calendar type */
function addNewEvent() {
    spinnerShow();
	
    var d = getFormData();
		
    // verify data, highlight errors	
	if (!verifyFormInputIsOK(d)) {
		$(".cssSpinner").hide();
		return false;
	}
	
	var newSharepointListItem = getSharepointListItem(d, "Events");
	
    //otherwise there will be an error when trying to save an existing event with a deleted event type, or simply an event without a selected event type
	if (newSharepointListItem.Type == "") { newSharepointListItem.Type = 0; }
    
	apiPostListContent("Events", newSharepointListItem, saveNewEventSuccess, saveOrUpdateEventError, { d: d });
}

/** hides modal screen and a little later hides its spinner */
function hideScreen(screenName) {
	switch (screenName) {
		case "create":
			$("#createScreen").fadeOut();
			break;
		case "filter":
			$("#filterScreen").fadeOut();
			break;
		case "edit":
			$("#editScreen").fadeOut();
			//reset all selected options in dropdowns
			$("#editScreen option[selected]").prop("selected", false);
			setTimeout(restoreDeleteLink, 600);
			break;
	    case "firstRun":
	        $("#firstRunScreen").fadeOut();
            //destroy as we won't need it anymore
            setTimeout(function () {
	            $("#firstRunScreen").remove();
	        }, 3000);
	        break;
	    case "bookRoom":
	        $("#bookRoomScreen").fadeOut();
	        $("#bookRoomScreen .cc-button-danger").hide(); //hide delete button - not needed for new event
	        $("#bookRoomScreen button.cc-button-ok").html("Book"); //restore value from "Save", which is changed when opening Edit event window
	        //clear all fields
	        break;
        case "bookCar":
            $("#bookCarScreen").fadeOut();
            break;
	    case "settings":
            $("#cc-settings-screen").fadeOut();
	        break;
		default:
			console.log("Error hiding screen.");
	}
	$(".modalPopup").off("click"); //remove click outside screen to close attached in attachClickOutsideToClose()
	setTimeout(function () {
	        $(".cssSpinner").hide();
	        clearAllFormFields(screenName);
    }, 600);
    setTimeout(function() {
    	$(".wrongInput").removeClass("wrongInput");
    },600);
}

/** save edited Meeting event to database and on screen */
function saveEditedEvent() {
	spinnerShow();
		
	var d = getFormData();
		
    // verify data, highlight errors	
	if (!verifyFormInputIsOK(d)) {
		$(".cssSpinner").hide();
		return;
	}
	
	var newSharepointListItem = getSharepointListItem(d, "Events");

    //otherwise there will be an error when trying to save an existing event with a deleted event type, or simply an event without a selected event type
	if (newSharepointListItem.Type == "") { newSharepointListItem.Type = 0; }
    
    // permanently save item in sharepoint
	apiUpdateListContent("Events", newSharepointListItem, updateEventSuccess, saveOrUpdateEventError, { d: d });
}

/** shows all css spinners simultaneously */
function spinnerShow() {
	$('.cssSpinner').show().css("display", "inline-block");
}

/** called if user clicked "No" on "Really delete?" */
function restoreDeleteLink() {
    $(".deleteEventLink").slideDown();
    $(".reallyDelete").slideUp();
}

/** deletes Meeting event from database and screen -- possibly DEPRECATED */
function deleteEventDEPRECATED(eventId) {
	var url = globalSharepointUrl + "_api/web/lists/getbytitle('_TravelEvent')/items(" + eventId + ")";
	spinnerShow();	
	$.ajax({
        url: url,
    	type: "POST",
    	contentType: "application/json; odata=verbose",
    	headers: {
    		"Accept": 			"application/json; odata=verbose",
    		"X-RequestDigest": 	globalRequestDigest,
    		"IF-MATCH": 		"*",
    		"X-HTTP-Method":	"DELETE"
    	},
    	success: function(d) {
    		C.fullCalendar('removeEvents', eventId);
    		hideScreen("edit");
    	},
    	error: function(d) {
    		console.log("Error deleting event from server.");
    		console.log(d);
    		showFormError("Error deleting event. Are you connected to the Internet?");
            $(".cssSpinner").hide();
        }
	});
	
}

/** returns travel member id by their full name; possibly deprecated TODO */
function getIdByName(name) {
    var fullNameId	= 	$.grep(globalTravelMembers, function(item) {
    						return item.Title === name;
    					});
    if (fullNameId.length === 0) { 
    	console.log("No such name found.");
    	return null;
    }
    return fullNameId[0].Id;
}

/** return location id by its full name; possibly deprecated TODO */
function getIdByLocation(location) {
	var locationId = $.grep(globalLocations, function(item) {
						return item.Title === location;
					});
	if (locationId.length === 0) {
		console.log("No such location found.");
		return null;
	}
	return locationId[0].Id;
}

/** display self-destructing error message above the buttons in the popup window */
function showFormError(msg) {
	$(".formErrorMessage").html(msg).show().delay(2500).fadeOut(2000);
}

/** highlights an invalid form field */
function makeRed(cssID) {
	$(cssID).addClass("wrongInput");
	
	/* remove red highlighting from invalid form field when its value is changed by user */
	/* special treatment for custom dropdown because it doesn't fire change event */
	if ($(cssID).hasClass("es-input")) {
		$(cssID).blur(function() { $(this).removeClass("wrongInput"); });
	}
	/* remove highlighting from both start and end date after either one is edited */
	else if ($(cssID).hasClass("formStartDate") || $(cssID).hasClass("formEndDate")) {
	    $(cssID).change(function () {
	        $(".formStartDate, .formEndDate").removeClass("wrongInput");
        });
	}
	else {
		$(cssID).change(function() { $(this).removeClass("wrongInput"); });
	}

	

}

/** intelligently formats tooltip date text depending on the event duration */
function getTooltipDateText(start, end) {
	//1 day or more
    var dash = " &ndash; ";
    var duration = end.format("x") - start.format("x");
	
	//1 day fitting starting and ending on 00:00 - 25 May 
	if (duration === 86400000 && start.format("HH:mm") === "00:00") {
		return start.format('DD MMM');	
	}
	//1 day or more - 25 May - 27 May
	var newEnd = end;
	if (duration >= 86400000) { //24 hrs
		if (end.format("HH:mm") === "00:00") {
			//otherwise will show next day; need to work on a copy //TODO check again, this may have changed after bug fix in line above
			newEnd = moment(end);
			newEnd.subtract(1, 'seconds');
		} 
		return start.format('DD MMM') + dash + newEnd.format('DD MMM');
	}
	
	//if event entirely in the first half of day, output date + AM
	if (
        start.format("H") >= 0 &&
        start.format("H") < 12 &&
        end.format("H") <= 12 &&
        end.format("H") > 0 &&
        (duration <= 43200000) //12 hrs, without this 1 jun 06:00 - 2 jun 02:00 will show as "AM"
        ) {
		return start.format('DD MMM') + " AM";
	}
	
	//if event entirely in the second half of day, output date + PM
	if (start.format("H") >= 12 &&
        start.format("H") < 24 &&
        end.format("H") <= 24 &&
        (end.format("H") > 12 || end.format("H") == 0)
        ) {
		return start.format('DD MMM') + " PM";
	}
	
	//less than one day, but non-standard time, e.g. 13:00 - 01:30
	return start.format('DD MMM HH:mm') + dash + end.format('DD MMM HH:mm');
}

/** fetches events for fullCalendar rendering */
function fetchEvents(start, end, timezone, callback) {
    // make a large window of data in order to avoid missing long-events on the calendar - they won't load otherwise
    start.subtract(2, 'months');
    end.add(2, 'months');

    start = start.format("YYYY-MM-DD") + "T" + start.format("HH:mm:ss");
    end = end.format("YYYY-MM-DD") + "T" + end.format("HH:mm:ss");

    //these parameters will be passed two functions further to make the ajax call to event
    var nextFunctionArgs = [start, end];

    //load travel types first so that we can assign colors to events    
    apiFetchListContent("Event_Types",
        "$filter=InstanceIdentifier eq '" + instanceId + "'&$top=4999",
        fetchTravelTypesSuccess,
        fetchError,
        callback,
        nextFunctionArgs);
}

/** fetches resources for fullCalendar rendering, or gets them from cache */
function fetchResources(fullCalendarCallback) {
    //do not async query server if resources were previously loaded
    //reload resources from cache and filter them at the same time
    
    //TODO need to refresh if dynamically added/removed a resource
    /*
    if (originalResourcesSnapshot.length > 0) {
        fullCalendarCallback(originalResourcesSnapshot);
    }
    */

    //no resources for simple schedule view
    if (getCalType() === "meetings") {
        var d = { d: { results: [] } };
        fetchResourcesSuccess(d, fullCalendarCallback);
    }

    //else, fetch resources from server    
    apiFetchListContent(
            "Resources",
            "$filter=InstanceIdentifier eq '" + instanceId + "'&$top=4999",
            fetchResourcesSuccess,
            fetchError,
            fullCalendarCallback
        );

    populateFilterCheckboxes();

}

/** fetches Sharepoint list content by list name, with/without options */
function apiFetchListContent(listName, urlOptions, callbackSuccess, callbackError, fullCalendarCallback, nextFunctionArgs) {
    var url = globalSharepointUrl + "/_api/web/lists/GetByTitle";
    if (urlOptions) {
        url += "('" + listName + "')/items?" + urlOptions;
    }
    else {
        url += "('" + listName + "')/items";
    }

    $.ajax({
        url: url,
        type: "GET",
        headers: {
            "accept": "application/json; odata=verbose"
        },
        success: function (d) {
            callbackSuccess(d, fullCalendarCallback, nextFunctionArgs);
        },
        error: function (d) {
            callbackError(d);
        }
    });
}

/** posts data to Sharepoint list by list name */
function apiPostListContent(listName, dataToPost, callbackSuccess, callbackError, nextFunctionArgs) {
    $.ajax({
        url: globalSharepointUrl + "/_api/web/lists/getbytitle('" + listName + "')/items",
        type: "POST",
        contentType: "application/json; odata=verbose",
        data: JSON.stringify(dataToPost),
        headers: {
            "Accept": "application/json; odata=verbose",
            "X-RequestDigest": globalRequestDigest
        },
        success: function (d) {
            callbackSuccess(d, nextFunctionArgs);
        },
        error: function (d) {
            callbackError(d);
        }
    });
}

/** updates a single item in a list on server using HTTP MERGE */
function apiUpdateListContent(listName, dataToPost, callbackSuccess, callbackError, nextFunctionArgs) {
    $.ajax({
        url: globalSharepointUrl + "/_api/web/lists/getbytitle('" + listName + "')/items(" + nextFunctionArgs.d.hiddenEventId + ")",
        type: "POST",
        contentType: "application/json; odata=verbose",
        data: JSON.stringify(dataToPost),
        headers: {
            "Accept": 			"application/json; odata=verbose",
            "X-RequestDigest": 	globalRequestDigest,
            "IF-MATCH": 		"*",
            "X-HTTP-Method":	"MERGE"
        },
        success: function (d) {
            callbackSuccess(d, nextFunctionArgs);
        },
        error: function (d) {
            callbackError(d, nextFunctionArgs);
        }
    });
}

/** deletes a single item from a list on the server */
function apiDeleteListContent(listName, itemId, callbackSuccess, callbackError, nextFunctionArgs) {
    /* rework */
    spinnerShow(); //TODO move to the calling function
    $.ajax({
        url: globalSharepointUrl + "/_api/web/lists/getbytitle('" + listName + "')/items(" + itemId + ")",
        type: "POST",
        contentType: "application/json; odata=verbose",
        headers: {
            "Accept": "application/json; odata=verbose",
            "X-RequestDigest": globalRequestDigest,
            "IF-MATCH": "*",
            "X-HTTP-Method": "DELETE"
        },
        success: function (d) {
            callbackSuccess(d, itemId);
        },
        error: function (d) {
            callbackError(d);
        }
    });
}

/** after fetching travel types, fetch events and add them to calendar */
function fetchTravelTypesSuccess(travelTypeData, fullCalendarCallback, nextFunctionArgs) {
    //save travel types to global variable
    globalTravelTypes = travelTypeData.d.results;
    populateTravelTypes();

    //now fetch the events
    apiFetchListContent(
            "Events",
            "$select=Title,Id,Type,Attendees,StartDate,EndDate,IsFullDay,Requestor,Comment,HandledBy&" +
            //"$expand=Attendees&" +
            "$filter=InstanceIdentifier eq '" + instanceId + "'" +
            "and StartDate ge datetime'" + nextFunctionArgs[0] + "' " + /* start of events window */
            "and EndDate le datetime'" + nextFunctionArgs[1] + "'" + /* end of events window */
            "&$top=" + ((72 - 41 - 1)*100).toString()
            ,
            fetchEventSuccess,
            fetchError,
            fullCalendarCallback
        );
}

/** shows an error message if any list data could not be fetched */
function fetchError(data) {
    //TODO show error when fetching ajax
    console.log("Error fetching data from server!");
    console.log(data);
}

/** reconstructs an array of fullCalendar events from server-fetched data and loads them into fullCalendar */
function fetchEventSuccess(eventsFromServer, fullCalendarCallback) {
    eventsFromServer = eventsFromServer.d.results;

    //construct an array of fullcalendar events from returned data and send it to fc's callback function
    var reconstructedEvents = [];

    for (var i = 0; i < eventsFromServer.length; i++) {
        reconstructedEvents.push({
            id: eventsFromServer[i].Id,
            resourceId: eventsFromServer[i].Title,
            travelTypeId: eventsFromServer[i].Type,
            title: eventsFromServer[i].Requestor, //TODO should be subject for room bookings? or just remove subject from room bookings entirely
            booker: eventsFromServer[i].Requestor,
            notes: eventsFromServer[i].Comment,
            attendees: eventsFromServer[i].Attendees,
            start: moment(eventsFromServer[i].StartDate),
            end: moment(eventsFromServer[i].EndDate),
            color: getColorCodeByTravelTypeId(eventsFromServer[i].Type)
        });
    }
    fullCalendarCallback(reconstructedEvents);

    /* populate settings window now because resources and colors were not available before that */
    globalPopulateSettingsStatus.colorsLoaded = true;
    attachAndPopulateSettings();
}

/** returns color code for event background from a numeric travel type ID, e.g. 1 => #ff0000 */
function getColorCodeByTravelTypeId(typeId) {
        var foundColor = ($.grep(globalTravelTypes, function (ttdItem) {
            return ttdItem.Id == typeId;
        }));

        //if no such travel group / color, return default color, else return actual color code
        if (foundColor.length === 0) {
            return "#0071bd"; //default color, ms blue
        }
        else {
            return foundColor[0].ColorCode;
        }
}

/** recreates an array of resources from server-fetched data and loads them into fullCalendar */
function fetchResourcesSuccess(resourcesData, fullCalendarCallback) {
    resourcesData = resourcesData.d.results;
    var returnedObjects = [];

    if (resourcesData === null) {
        returnedObjects = null;
    }
    else {
        /* recreate resources array from data fetched from Sharepoint */
        for (var i = 0; i < resourcesData.length; i++) {
            returnedObjects.push({
                id: resourcesData[i].Id,

                //if no name defined for resource, output "no name" instead of simply its Id / null
                title: (!resourcesData[i].Title ? "(no name " + resourcesData[i].Id + ")" : resourcesData[i].Title),
                department: resourcesData[i].Group,
                description: resourcesData[i].ResourceDescription,
                order: resourcesData[i].SortingOrder
            });
        }

        /* sort the array of resources by the SortingOrder */
        returnedObjects.sort(function (a, b) {
            return parseInt(a.order) - parseInt(b.order);
        });
    }

    /* store resources in global variable to restore them later */
    originalResourcesSnapshot = returnedObjects;

    /* populate resources dropdowns in create/edit windows */
    populateResourcesDropdown();

    /* fullcalendar's function */
    fullCalendarCallback(returnedObjects);

    /* populate settings window now because resources and colors were not available before that */
    globalPopulateSettingsStatus.resourcesLoaded = true;
    attachAndPopulateSettings();
}

/** sets calendar options and renders the calendar */
function createFullCalendar() {
    // do no show anything when app part is in page edit mode
    /* this and other methods don't work because SP scripts are slow to load
    if (typeof g_disableCheckoutInEditMode !== "undefined" && g_disableCheckoutInEditMode) {
        C.html("Page is in edit mode. Save the page to see the calendar.")
        return;
    }
    */
    C.fullCalendar({
        schedulerLicenseKey: schedulerLicenseKey,
        firstDay: 1,
        aspectRatio: 1.8,
        editable: true,
        resizable: true,
        lazyFetching: true,
        eventOverlap: getEventOverlap(),
        selectable: false,
        buttonText: getFullCalendarButtonText(),
        resourceLabelText: getResourceLabelText()
        , allDayDefault: false
        , scrollTime: '00:00' /* for agenda view */
        , timeFormat: ' '
        , header: {
            left: 'settingsButton leftCreateButton'
            , center: 'prev, title, next'
            , right: 'createButton,filterButton, ' + getFullCalendarViewButtons()
        }
        , defaultView: getFullCalendarDefaultView()
        , views: getFullCalendarViews()
        , customButtons: {
            createButton: {
                text: getCreateButtonText()
                , click: checkIfCanShowCreateScreen
            }
            , filterButton: {
                text: " Filter"
                , click: showFilterScreen
            }
            , settingsButton: {
                text: "", /* = three gears,  = one gear */
                click: showSettings
            },
            leftCreateButton: {
                text: getCreateButtonText(), /*  = plus sign in a square */
                click: checkIfCanShowCreateScreen
            }
        }
        , resourceGroupField: 'department'
        , resources: function (callback) {
            fetchResources(callback);
        }
        , events: function (start, end, timezone, callback) {
            fetchEvents(start, end, timezone, callback);
        }
        , eventClick: function (event/*, jsEvent, view*/) {
            checkIfCanShowEditScreen(event);
        }
        , dayClick: function (date, jsEvent, view) {
            console.log(jsEvent);
            if (!checkBrowserTouchSupport()) {
                return; /* otherwise click while touch scrolling */
            }
            if (isDraggingOnMobile) {
                isDraggingOnMobile = false;
                return;
            }
            checkIfCanShowCreateScreen(date);
            //clickedCalendarDate = date;
            //C.on("mousemove", forgetSlot); 
        },
        viewRender: function (view) {
            //nothing
        }
        , eventRender: function (event, element) {
            // don't render if event is filtered out
            /* filters disabled
            if (!isEventFilteredIn()) {
                return false;
            }
            */

            // add tooltip
            $(".qtip").hide();
            addToolTip(element, { title: getEventTypeById(event.travelTypeId), text: getEventToolTip(event, element) });

            // replace the title from booker to notes
            element[0].innerHTML = element[0].innerHTML.replace(
                '<span class="fc-title">' + (event.title || "&nbsp;") + "</span>",
                '<span class="fc-title">' + getEventDisplayText(event, element) + "</span>"); //added span to avoid accidentally replacing wrong stuff
        }
        , resourceRender: function (resourceObj, labelTds, bodyTds) {
            addToolTip(labelTds, {
                title:  resourceObj.department,
                text:   resourceObj.title /*text: resourceObj.description*/
            }, "mouse");
        },
        eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {
            eventDropOrResizeHandler(event, delta, revertFunc, jsEvent, ui, view);
        },
        eventResize: function (event, delta, revertFunc, jsEvent, ui, view) {
            eventDropOrResizeHandler(event, delta, revertFunc, jsEvent, ui, view);
        }
    }); //fullcalendar end
    //refresh the calendar every 25 minutes to see if there are new events
    setInterval(function () {
        C.fullCalendar('refetchEvents');
    }, 25 * 60 * 1000);
}

/** adds a tooltip to a specified element, like an event or resource */
function addToolTip(element, o, target, hideEvents) {
    if (target === undefined || target === null) {
        target = false;
    }
    if (hideEvents === undefined) {
        hideEvents = "mousedown mouseup mouseleave";
    }
    element.qtip({
        content: {
            title: o.title,
            text: o.text
        }
        , position: {
            my: "bottom center",
            at: "top center",
            target: target // "mouse" is buggy when dragging and interferes with clicking
            //adjust: { y: -9}
        }
        , style: {
            tip: { corner: 'bottom center' },
            classes: 'qtip-light qtip-shadow'
        },
        show: {
            effect: function () {
                $(this).fadeTo(300, 1);
            },
            solo: true,
            delay: 200
        },
        hide: {
            effect: true,
            event: hideEvents // otherwise stays while dragging
        }
    });
}

/** makes a synchronous query to list to check whether the calendar type option is set; if not, it's the first time we run the calendar */
function isFirstRun() {

    $.ajax({
        async: false,
        url: globalSharepointUrl + "/_api/web/lists/getbytitle('Settings')/items?$filter=InstanceIdentifier eq '" + instanceId + "'",
        type: "GET",
        headers: {
            "accept": "application/json; odata=verbose"
        },
        success: function(d) {
            globalCalendarSettings = d.d.results[0];
        },
        error: function(d) {
            $("#firstRunScreen .buttonRow-right").append(
                "<span class='cc-setting-error-saving'><br><br>" +
                "Error reading settings from server. Reload this page or check that you have access rights to lists in Sharepoint</span>");
            console.log("Error fetching settings from server.");
            console.log(d);
        }
    });

    if (!globalCalendarSettings) {
        globalCalendarSettings = { CalendarType: "" };
        return true;
    }

    if (globalCalendarSettings.CalendarType !== "meetings" &&
        globalCalendarSettings.CalendarType !== "roombooking" && 
        globalCalendarSettings.CalendarType !== "carbooking") {
        return true; //first time run
    }
    else {
        return false;
    }
}

/** shows First Run initial setup screen and attaches some UX functions */
function showFirstRunScreen() {
    //attach click handlers for options for better ux
    $(".calendarTypeOption div").click(function () {
        $(this).siblings("p").find("input").prop("checked", "true").change(); //also trigger change event for OK button to be enabled
        //show/hide description when an option is selected
        $(".calendarTypeDescription").css("color", "white");
        $(this).siblings(".calendarTypeDescription").css("color", "#666");
    });

    $(".calendarTypeOption div").hover(function () {
        $(this).siblings(".calendarTypeDescription").css("color", "#666");
    }, function () {
        if ($(this).siblings("p").find("input").prop("checked") !== true) {
            $(this).siblings(".calendarTypeDescription").css("color", "white");
        }
    });

    $(".calendarTypeOption p").click(function () {
        $(this).find("input").prop("checked", "true").change();
        //show/hide description when an option is selected
        $(".calendarTypeDescription").css("color", "white");
        $(this).siblings(".calendarTypeDescription").css("color", "#666");
    });

    //attach Finish button click handler
    $("#firstRunScreen button.cc-button-ok").click(function () {
        saveCalendarType();
        createSampleEventsOnFirstRun(); //TODO some events may not render because creating them takes time
        createFullCalendar();
        doAfterCalendarLoadAttachments();
        hideScreen("firstRun");
        //$("#firstRunScreen > div > div > div").animate({ marginLeft: '-400%' });
    });

    //disable button until an option is selected
    $("#firstRunScreen input").change(function() {
        $("#firstRunScreen .cc-button-ok").prop("disabled", false);
    });
    
    //show modal screen
    $("#firstRunScreen").fadeIn();
}

function resetSavedCalendarType() {
    apiDeleteListContent("Settings", 1, function () { }, function () { });
    apiDeleteListContent("Settings", 2, function () { }, function () { });
    apiDeleteListContent("Settings", 3, function () { }, function () { });
} 

/** returns calendar type. Possible values: meetings, roombooking, carbooking */
function getCalType() {
    return globalCalendarSettings.CalendarType;
}

/** save calendar type selected in the first run screen */
function saveCalendarType() {
    var calendarType = $("#firstRunScreen input:checked").val();
    globalCalendarSettings.CalendarType = calendarType;
    var settingsToPost = {
        "__metadata": { "type": getListItemType("Settings") },
        "CalendarType": calendarType,
        "InstanceIdentifier": instanceId
    };
    apiPostListContent("Settings", settingsToPost, function (d) { console.log("Instance settings saved"); }, function (d) { console.log(d); }, { d: {hiddenEventId: 1}});
}

/** shows modal screen for creating a new room booking, with optional date */
function showCreateRoomBookingScreen(clickedDay) {
    outputCurrentUserName("#bookRoomScreen .currentUserName");

    attachRoomBookingScreenHandlers();

    // preset dates either based on the day that's clicked, or set some reasonable defaults if clicked +Create
    if (clickedDay !== undefined && clickedDay !== null && clickedDay._isAMomentObject === true) {
        $("#bookRoomScreen .formStartDate").val(moment(clickedDay).format('YYYY-MM-DD HH:mm'));
        $("#bookRoomScreen .formEndDate").val(moment(clickedDay).add(1, 'days').format('YYYY-MM-DD HH:mm'));
    } else {
        // set default dates to tomorrow and +2 days
        var tomorrow = moment(new Date).add(1, 'days').hours(8).minutes(0).seconds(0);
        $("#bookRoomScreen .formStartDate").val(tomorrow.format('YYYY-MM-DD HH:mm'));
        $("#bookRoomScreen .formEndDate").val(moment(tomorrow).add(1, 'days').format('YYYY-MM-DD HH:mm'));
    }

    attachClickOutsideToClose("bookRoom");

    $("#bookRoomScreen").fadeIn();
}

/** shows modal screen for creating a new vehicle booking, with optional date */
function showCarBookingScreen(date) {
    $("#bookCarScreen").fadeIn();
}

/** attaches jQuery actions to various buttons and UI elements on initial load, renders datetimepickers etc. */
function doAfterCalendarLoadAttachments() {
    attachSaveCancelButtons();
    attachButtonIcons();

    /* load names into dropdown in Create Event screen */
    populateNamesDropdown();
    populateLocationsDropdown();

    attachDateTimePickers();

    initializePeoplePicker("PeopleEditor1");

    // attach handle tab switching in Settings
    $(".cc-tab-bar button").click(function () {
        var tabToActivate = $(this).attr("data-tab-number");
        //hide all tabs, activate the one we need based on the button's data-tab-number attribute
        $(".cc-tab").hide();
        $(".cc-tab:nth-of-type(" + tabToActivate + ")").show();
        //switch tab button colors
        $(".cc-tab-bar button").removeClass("cc-tab-active");
        $(this).addClass("cc-tab-active");
    });

    //hide resources tab for simple view
    if (getCalType() === "meetings") {
        $(".cc-tab-bar button[data-tab-number=2]").hide();
    }

    //temporarily disable Other tab until more settings are implemented
    $(".cc-tab-bar button[data-tab-number=3]").hide();

    // attach escape key handlers
    $("body").keyup(function (e) {
        if (e.keyCode === 27) { // Esc 
            $("#createScreen button.cc-button-cancel").click();
            $("#filterScreen button.cc-button-cancel").click();
            $("#editScreen button.cc-button-cancel").click();
            $("#bookRoomScreen button.cc-button-cancel").click();
            $("#cc-settings-screen .cc-button-ok").click(); //OK = close, no save button
            //TODO handle the same for other create screens, settings screen
        }
        //TODO: handle Enter key IF a popup is open, keycode == 13, equivalent to clicking Save
    });

    /* select all / unselect all checkbox in the Filter screen */
    /*
    $("#toggleAll").click(function () {
        var checkboxes = $(".gradeFilterCheckboxes");
        var setTo = $("#toggleAll").is(":checked");

        for (var i = 0; i < checkboxes.length; i++) {
            $(checkboxes[i]).prop('checked', setTo);
        }
    });
    */

    $(".deleteEventLink").click(function () {
        $(".deleteEventLink").slideUp();
        $(".reallyDelete").slideDown();
    });

    $(".noDeleteEvent").click(function () {
        restoreDeleteLink();
    });

    $(".yesDeleteEvent").click(function () {
        spinnerShow();
        var eventId = $("#editScreen #spEventId").val();
        deleteEvent(eventId);
    });

    $(".formTravelType").change(function () {
        $(".cc-edit-form-color-sampler").css("background-color", getColorCodeByTravelTypeId($(this).val()));
    });

    //disable preserving filter state across page reloads
    window.localStorage["fcGradeFilterStorage"] = '["all"]';

    //check whether SP loaded and if so, check if user has edit rights
    //SP.SOD.executeFunc("sp.js", "SP.ClientContext", verifyUserAuthorizedToEdit);
    verifyUserAuthorizedToEdit();
}

/** populates dropdowns and attaches event listeners for settings window */
function attachAndPopulateSettings() {
    // 1. don't populate if both resources and colors not loaded yet; 2. don't populate and reattach actions if already populated earlier
    if (globalPopulateSettingsStatus.alreadyPopulated === true) { return; }
    if (globalPopulateSettingsStatus.colorsLoaded === false || globalPopulateSettingsStatus.resourcesLoaded === false) { return; }

    //populate resources tab in settings

    //TODO sort by resource group first - make a global function for sorting resources, copy from dropdown population
    //var r = sortResourcesArrayByGroup(originalResourcesSnapshot)
    var outputHtml = '';
    for (var i = 0; i < originalResourcesSnapshot.length; i++) {
        outputHtml +=
            '<div class="cc-res-row">' +
                '<input type="hidden" class="cc-setting-id" value="' + originalResourcesSnapshot[i].id + '">' +
                '<input type="text" readonly="readonly" class="cc-res-department" value="' + originalResourcesSnapshot[i].department + '" placeholder="E.g. building or department">' +
                '<input type="text" readonly="readonly" class="cc-res-resource" value="' + originalResourcesSnapshot[i].title + '" placeholder="E.g. meeting room or employee">' +
                '<input type="number" readonly="readonly" class="cc-res-order" value="' + originalResourcesSnapshot[i].order + '">' +
                '<button type="button" class="cc-delete-icon">⨯</button>' +
            '</div>';
    }
    $("#cc-settings-screen .cc-resources-list").html(outputHtml);

    //TODO possibly sort too
    outputHtml = '';
    for (var i = 0; i < globalTravelTypes.length; i++) {
        outputHtml +=
            '<div class="cc-colors-row">' +
                '<input type="hidden" class="cc-setting-id" value="' + globalTravelTypes[i].Id + '">' +
                '<input type="text" readonly="readonly" class="cc-colors-type" value="' +
                ((globalTravelTypes[i].EventTypeName === null || globalTravelTypes[i].EventTypeName === "null") ? "(no name)" : globalTravelTypes[i].EventTypeName) +
                '" placeholder="Enter a description for this color">' +
                '<span class="cc-colors-colorcode-sample" style="background-color: ' +
                ((globalTravelTypes[i].ColorCode === null || globalTravelTypes[i].ColorCode === "null") ? "grey" : globalTravelTypes[i].ColorCode) +
                ';"></span>' +
                '<input type="text" readonly="readonly" class="cc-colors-colorcode" value="' + globalTravelTypes[i].ColorCode + '" placeholder="E.g. indigo">' +
                '<button type="button" class="cc-delete-icon">⨯</button>' +
            '</div>';
    }
    $("#cc-settings-screen .cc-types-list").html(outputHtml);

    //on changing color, change color sample to the left
    $(".cc-types-list").on("keyup", ".cc-colors-colorcode", function () {
        $(this).change(); //otherwise change not firing
        $(this).siblings(".cc-colors-colorcode-sample").css("background-color", $(this).val().trim());
    });

    //attach handlers to settings list input field; mousedown because otherwise click doesn't result in text cursor appearing in textbox
    $(".cc-resources-list, .cc-types-list").on("mousedown click focus", ".cc-res-row input, .cc-colors-row input", function () {
        $(this).removeAttr("readonly");
    });

    //disable input on leaving it
    $(".cc-resources-list, .cc-types-list").on("blur", ".cc-res-row input, .cc-colors-row input", function () {
        $(this).prop("readonly", true);
        saveResourceOrColorAfterInlineEdit(this);
    });

    //attach action to 'delete' icon - confirm delete box for settings
    $(".cc-resources-list, .cc-types-list").on("click", ".cc-delete-icon", function (deleteClickEvent) {
        //if this is a newly created line, remove without confirming
        if ($(this).siblings(".cc-setting-id").val() == "") {
            $(this).parent().remove();
            deleteClickEvent.stopPropagation(); // otherwise the modal dialog closes automatically 
            return;
        }    

        //if confirm delete already open, do nothing - just close it and do not continue
        var boxAlreadyOpen = $(this).next().hasClass("cc-confirm-delete-setting");
        if (boxAlreadyOpen === true) {
            removeConfirmDeleteBoxes(100);
            return; //already closed all confirmations above
        }

        //first, remove any previous slidedowns
        removeConfirmDeleteBoxes();

        //show confirm delete box
        $(this).after(
            "<div class='cc-confirm-delete-setting'>" + 
            "<p>Warning: if you delete this resource, it will also delete all the events associated with it. " +
            "If you want to keep the events, you can rename it instead of deleting, or edit the events first.</p>" +
            "<p class='cc-buttonsRow-center'><button type='button' class='cc-button-cancel' " + 
            "data-item-id='" + $(this).siblings(".cc-setting-id").val() + "' " +
            "data-tab-name='" + ($(this).parent().hasClass("cc-res-row") ? "res" : "colors") + "'>Delete</button>" +
            "<button type='button' class='cc-button-ok'>Do not delete</button></p>" +
            "</div>"
        );
        $(".cc-confirm-delete-setting").slideDown(200);

        //cancel button action
        $(".cc-confirm-delete-setting .cc-button-ok").click(function () {
            removeConfirmDeleteBoxes(100);
        });

        //really delete button action
        $(".cc-confirm-delete-setting .cc-button-cancel").click(function () {
            deleteResourceOrColor({ id: $(this).attr('data-item-id'), settingType: $(this).attr('data-tab-name') });
        });

    });

    //click on color sampler to focus on color edit field
    $(".cc-types-list").on("click", ".cc-colors-colorcode-sample", function () {
        $(this).siblings(".cc-colors-colorcode").click().focus();
    });

    $(".cc-add-row-resources button").click(function () {
        //add new row only if the previous one is not empty; exception if zero rows in table - then add new row anyway
        if (
            $(".cc-resources-list").children().length === 0 ||
            ($(".cc-resources-list").children(":last-child").find(".cc-res-department").val().trim() !== "" &&
            $(".cc-resources-list").children(":last-child").find(".cc-res-resource").val().trim() !== "")
            ) {
            var newResourceRow =
                    '<div class="cc-res-row">' +
                    '<input type="hidden" class="cc-setting-id" value="">' +
                    '<input type="text" readonly="readonly" class="cc-res-department" value="" placeholder="E.g. building or department">' +
                    '<input type="text" readonly="readonly" class="cc-res-resource" value="" placeholder="E.g. meeting room or employee">' +
                    '<input type="number" readonly="readonly" class="cc-res-order" value="">' +
                    '<button type="button" class="cc-delete-icon">⨯</button>' +
                    '</div>';
            $(".cc-resources-list").append(newResourceRow);
        }
        var latestAddedRow = $(".cc-resources-list").children(":last-child");
        latestAddedRow.find(".cc-res-department").click().focus();
        attachSortingOrderToolTip(latestAddedRow.find(".cc-res-order"));
    });

    $(".cc-add-row-colors button").click(function () {
        if (
            $(".cc-types-list").children().length === 0 ||
            ($(".cc-types-list").children(":last-child").find(".cc-colors-type").val().trim() !== "" &&
            $(".cc-types-list").children(":last-child").find(".cc-colors-colorcode").val().trim() !== "")
            ) {
            var newColorsRow = 
                '<div class="cc-colors-row">' +
                    '<input type="hidden" class="cc-setting-id" value="">' +
                    '<input type="text" readonly="readonly" class="cc-colors-type" value="" placeholder="Enter a description for this color">' +
                    '<span class="cc-colors-colorcode-sample" style="background-color: transparent;"></span>' +
                    '<input type="text" readonly="readonly" class="cc-colors-colorcode" value="" placeholder="E.g. indigo">' +
                    '<button type="button" class="cc-delete-icon">⨯</button>' +
                '</div>'
            ;
            $(".cc-types-list").append(newColorsRow);
        }
        var latestAddedRow = $(".cc-types-list").children(":last-child");
        latestAddedRow.find(".cc-colors-type").click().focus();
        attachColorCodeToolTip(latestAddedRow.find(".cc-colors-colorcode"));
    });

    //add help tooltips for existing elements
    attachSortingOrderToolTip($(".cc-resources-list .cc-res-order"));
    attachColorCodeToolTip($(".cc-colors-colorcode"));

    //link to view license info 
    $("#cc-more-link").click(function () {
        $("#gplv3details").slideToggle();
    });

    globalPopulateSettingsStatus.alreadyPopulated = true;
}

/** removes all 'confirm delete' boxes in the settings window; optional duration for slideup effect */
function removeConfirmDeleteBoxes(duration) {
    if (duration !== undefined) {
        $(".cc-confirm-delete-setting").slideUp(duration);
        setTimeout(function () { $(".cc-confirm-delete-setting").remove(); }, duration);
    }
    else {
        $(".cc-confirm-delete-setting").remove();
    }
}

/** on first run, creates a few sample events on today's week */
function createSampleEventsOnFirstRun() {
    // only create sample events once, for the default calendar
    if (instanceId !== "Default") {
        return;
    }

    var thisWeeksMonday = moment(new Date()).weekday(1).hours(0).minutes(0);

    var samples = {
        data: [{
            startDate:  moment(thisWeeksMonday).add(15, 'hours').format("YYYY-MM-DD HH:mm"),
            endDate:    moment(thisWeeksMonday).add(44, 'hours').format("YYYY-MM-DD HH:mm"),
            type: '6',
            subject: "Andrew Reiss",
            attendees: '',
            notes: 'As discussed last week',
            booker: "Andrew Reiss",
            resourceId: '1'
        },
        {
            startDate:  moment(thisWeeksMonday).add(3, 'days').add(8, 'hours').format("YYYY-MM-DD HH:mm"),
            endDate:    moment(thisWeeksMonday).add(3, 'days').add(23, 'hours').format("YYYY-MM-DD HH:mm"),
            type: '2',
            subject: "Anna Jones",
            attendees: '',
            notes: 'Business travel to customer office',
            booker: "Anna Jones",
            resourceId: '3'
        },
        {
            startDate:  moment(thisWeeksMonday).add(20, 'hours').format("YYYY-MM-DD HH:mm"),
            endDate:    moment(thisWeeksMonday).add(3, 'days').add(20, 'hours').format("YYYY-MM-DD HH:mm"),
            type: '1',
            subject: "Meg Colin",
            attendees: '',
            notes: 'Out for two-day training',
            booker: "Meg Colin",
            resourceId: '2'
        },
        {
            startDate:  moment(thisWeeksMonday).add(4, 'days').add(20, 'hours').format("YYYY-MM-DD HH:mm"),
            endDate:    moment(thisWeeksMonday).add(6, 'days').add(20, 'hours').format("YYYY-MM-DD HH:mm"),
            type: '4',
            subject: "Rodger Sowards",
            attendees: '',
            notes: 'Not in office',
            booker: "Rodger Sowards",
            resourceId: '4'
        },
        {
            startDate:  moment(thisWeeksMonday).add(3, 'days').add(20, 'hours').format("YYYY-MM-DD HH:mm"),
            endDate:    moment(thisWeeksMonday).add(5, 'days').add(20, 'hours').format("YYYY-MM-DD HH:mm"),
            type: '5',
            subject: "Robert Leith",
            attendees: '',
            notes: '',
            booker: "Robert Leith",
            resourceId: '1'
        }]
    };

    for (var i = 0; i < samples.data.length; i++) {
        var newSharepointListItem = getSharepointListItem(samples.data[i], "Events");
        apiPostListContent("Events", newSharepointListItem, function (d) { /*console.log(d);*/ }, function (d) { console.log("Error creating event."); console.log(d); }, {});
    }
}

/** gets values from new room booking form, validates and saves new booking */
function saveRoomBooking() {
    spinnerShow();

    //pull user-filled form data
    var d = getFormData();

    //verify form data, show and highlight errors to user, stop if error
    if (!verifyFormInputIsOK(d)) {
        $(".cssSpinner").hide();
        return false;
    }

    //create new sharepoint list item before saving it to server
    var newSharepointListItem = getSharepointListItem(d, "Events");

    //otherwise there will be an error when trying to save an existing event with a deleted event type, or simply an event without a selected event type
    if (newSharepointListItem.Type == "") { newSharepointListItem.Type = 0; }

    //check whether we were in the New screen or Edit screen 
    if (d.hiddenEventId === "" || d.hiddenEventId === undefined) {
        //save new item to server
        apiPostListContent("Events", newSharepointListItem, saveNewEventSuccess, saveOrUpdateEventError, { d: d });
    }
    else {
        //update existing item on server
        apiUpdateListContent("Events", newSharepointListItem, updateEventSuccess, saveOrUpdateEventError, { d: d });
    }
}

/** returns MS Sharepoint list item type required for POST requests based on list name; will fail if first letter of list is not capitalized */
function getListItemType(listName) {
    var x = "SP.Data." + listName + "ListItem";
    x = x.replace("_", "_x005f_"); //weird sharepoint bug, underscore needs to replaced to this
    return x;
}

/** shows settings screen after clicking on the hamburger menu */
function showSettings() {
    $("#cc-settings-screen").fadeIn();
    //attach click outside screen to close
    attachClickOutsideToClose("settings");
}

/** sets Sharepoint people picker value; hack without using API calls to server; TODO will not work in a non-english sharepoint */
/* better implementation here: https://msdn.microsoft.com/en-us/library/office/jj713593.aspx */
function setPeoplePicker(peoplePickerId, valueToSet) {
    var opt = {
        peoplePickerId: peoplePickerId,
        valueToSet: valueToSet,		// The value to set the People Picker to. Should be a string containing each username or groupname separated by semi-colons.
        checkNames: true			// If set to true, the Check Names image will be clicked to resolve the names
    };

    if (opt.valueToSet === undefined || opt.valueToSet === null) {
        opt.valueToSet = "";
    }

    opt.valueToSet = opt.valueToSet.split("; ");

    //new implementation for new style SP control
    for (var i = 0; i < opt.valueToSet.length; i++) {
        (this.SPClientPeoplePicker.SPClientPeoplePickerDict[peoplePickerId + "_TopSpan"]).AddUserKeys(opt.valueToSet[i]);
    }
    // Find the row containing the People Picker - for old style people picker
    /*
    var thisRow = $("span").filter("span[id*=_" + opt.peoplePickerId + "]");
    var thisContents = thisRow.find("div[Title='People Picker']");
    var thisCheckNames = thisRow.find("img[Title='Check Names']:first");
    if (opt.valueToSet.length > 0) thisContents.html(opt.valueToSet);
    if (opt.checkNames) thisCheckNames.click();
    var thisCurrentValue = thisContents.text();
    */

    return;
}

/** Clears Sharepoint people picker */
function clearPeoplePicker(peoplePickerElementId) {
    var pp = this.SPClientPeoplePicker.SPClientPeoplePickerDict[peoplePickerElementId + "_TopSpan"];

    while (pp.TotalUserCount > 0) {
        pp.DeleteProcessedUser();
    }

    $("#" + peoplePickerElementId + " .sp-peoplepicker-editorInput").val("");
}

/** Initialize people picker via JS instead of directly in code because otherwise it is not working within an iframe/app part */
function initializePeoplePicker(peoplePickerElementId) {
    var schema = {};
    schema['PrincipalAccountType'] = 'User,DL,SecGroup,SPGroup';
    schema['SearchPrincipalSource'] = 15;
    schema['ResolvePrincipalSource'] = 15;
    schema['AllowMultipleValues'] = true;
    schema['MaximumEntitySuggestions'] = 5;
    schema['Width'] = '210px';
    schema['Height'] = '88px'; /* doesn't seem to work, using CSS */

    this.SPClientPeoplePicker_InitStandaloneControlWrapper(peoplePickerElementId, null, schema);

    $(".sp-peoplepicker-topLevel").click(function (event) {
        event.stopPropagation(); /* otherwise modal dialog closes */
        return true;
    });
}

/** returns semicolon-separated keys or plaintext names from the people picker, e.g. keys like i:0#.f|membership|test@example.com; i:0#.f|membership|test3@example.com; */
function getPeoplePickerNames(peoplePickerId, returnPlaintextNames) {
    var names = '';

    var attrToReturn = "Key";
    if (returnPlaintextNames === true) {
        attrToReturn = "DisplayText";
    }
    var hiddenInputValue = $("input[id*=" + peoplePickerId + "_TopSpan_HiddenInput]").val();
    
    if (hiddenInputValue != "") {
        hiddenInputValue = JSON.parse(hiddenInputValue);
    }

    for (var i = 0; i < hiddenInputValue.length; i++) {
        names += hiddenInputValue[i][attrToReturn] + "; ";
    }

    return names;

    /* for the old type of people picker
    var nameDivs = $("[id*=_" + peoplePickerId + "]").find("div#divEntityData");
    
    var attrToReturn = "key";
    if (returnPlaintextNames === true) {
        attrToReturn = "displaytext";
    }
    for (var i = 0; i < nameDivs.length; i++) {
        names += $(nameDivs[i]).attr(attrToReturn);
        names += "; ";
    }
    */
}

/** TODO returns a resource's name from its id, e.g. Room A from 2 */
function getResourceNameById(resourceId) {
    for (var i = 0; i < originalResourcesSnapshot.length; i++) {
        if (originalResourcesSnapshot[i].id == resourceId) {
            return originalResourcesSnapshot[i].title;
        }
    }
    return "";
}

/** TODO returns a resource's name from its id, e.g. Room A from 2 */
function getResourceDescriptionById(resourceId) {
    for (var i = 0; i < originalResourcesSnapshot.length; i++) {
        if (originalResourcesSnapshot[i].id == resourceId) {
                return originalResourcesSnapshot[i].description;
            }
        }
    return "";
}

/** returns an event type by its ID, e.g. "Annual leave" from 2 */
function getEventTypeById(eventType) {
    var result = $.grep(globalTravelTypes, function (item) {
        return parseInt(item.Id) === parseInt(eventType);
    });
    if (result.length > 0) {
        return result[0].EventTypeName;
    }
    else {
        return "";
    }
}

/** attaches jquery actions to create/edit room booking screen */
function attachRoomBookingScreenHandlers() {
    //attach resource description display when <select> resource is changed
    $("select.resourceSelect").change(function () {
        var thisResourceId = $(this).val();
        $(".resourceDescription").html(getResourceDescriptionById(thisResourceId));
        
    });

    // attach edit link hanler - make input editable when clicking, uneditable on blur
    $(".cc-edit-link").click(function () {
        $(".currentUserName").removeAttr("disabled");
        $(".currentUserName").focus();
    });

    $(".currentUserName").blur(function () {
        $(".currentUserName").prop("disabled", "disabled");
    });

}

/** after updating an event on server, update it on screen too */
function updateEventSuccess(dataFromServer, originalDataToPost) {
    originalDataToPost = originalDataToPost.d;

    // fetch fullcalendar event object in order to update it on screen 
    var eventToModify = C.fullCalendar('clientEvents', originalDataToPost.hiddenEventId);
    eventToModify = eventToModify[0];

    eventToModify.attendees     = originalDataToPost.attendees;
    eventToModify.booker = originalDataToPost.booker;
    eventToModify.color = getColorCodeByTravelTypeId(originalDataToPost.type);
    eventToModify.end = moment(originalDataToPost.endDate);
    eventToModify.hiddenEventId = originalDataToPost.hiddenEventId;
    eventToModify.notes = originalDataToPost.notes;
    eventToModify.resourceId = originalDataToPost.resourceId;
    eventToModify.start = moment(originalDataToPost.startDate);
    eventToModify.subject = originalDataToPost.subject;
    eventToModify.travelTypeId = originalDataToPost.type;

    //now show the updated event on screen
    C.fullCalendar('updateEvent', eventToModify);
    C.fullCalendar('refetchEvents');//TODO hack, shouldn't be this way, too slow
    hideScreen("bookRoom"); //TODO FIXME just hide all visible in one jquery call
    hideScreen("create");
    hideScreen("edit");
}

/** called when error updating event on server */
function saveOrUpdateEventError(d, originalDataToPost) {
    showFormError("Error saving event. Are you connected to the Internet?");
    $(".cssSpinner").hide();
    console.log("Error saving/updating event."); 
    console.log(d);
    console.log(originalDataToPost);
}

/** deletes event from server, if success then also from screen */
function deleteEvent(eventId) {
    apiDeleteListContent("Events", eventId, deleteEventSuccess, deleteEventError, {});
}

/** after event is deleted from server */
function deleteEventSuccess(datafromServer, itemId) {
    C.fullCalendar('removeEvents', itemId);
    hideScreen("bookRoom");
    hideScreen("edit");
}

/** error deleting event on server */
function deleteEventError(datafromServer) {
    console.log("Error deleting event from server.");
    console.log(datafromServer);
    showFormError("Error deleting event. Are you connected to the Internet?");
    $(".cssSpinner").hide();
}

/** save resource/color after inline editing */
function saveResourceOrColorAfterInlineEdit(clickTarget) {
    //first, figure out whether we are in Resources or Colors window
    var clickTargetParent = $(clickTarget).parent();

    //create space for the success/error icon; these will be then removed from DOM
    clickTargetParent.prepend(
		"<span class='cc-setting-save-status-icon'></span>"
		);

    if (clickTargetParent.hasClass("cc-res-row")) {
        var newResourceToPost = {
            id: clickTargetParent.children().filter(".cc-setting-id").val(),
            department: clickTargetParent.children().filter(".cc-res-department").val().trim(),
            resource: clickTargetParent.children().filter(".cc-res-resource").val().trim(),
            order: clickTargetParent.children().filter(".cc-res-order").val()
        };

        //validate data 
        if (newResourceToPost.department == "" && newResourceToPost.resource == "") { return;} //do not post if user filled in nothing
        if (newResourceToPost.order == "") { newResourceToPost.order = 1; }

        var newSharepointListItem = {
            "__metadata": { "type": getListItemType("Resources") },
            "Title": newResourceToPost.resource,
            "Group": newResourceToPost.department,
            "SortingOrder": newResourceToPost.order,
            "InstanceIdentifier": instanceId
        };
        
        //copy id if item already exists
        if (newResourceToPost.id != "") {
            newSharepointListItem.Id = newResourceToPost.id;
        }
        
        //if no id, post new item; if id is not empty, then just update existing item
        if (newResourceToPost.id != "") {
            apiUpdateListContent(
                "Resources",
                newSharepointListItem,
                function (d) {
                    showSettingSavedOKIcon();
                    removeAllSettingSavedIcons();
                    C.fullCalendar('refetchResources');
                },
                function (d) {
                    showSettingErrorSavingIcon();
                    removeAllSettingSavedIcons();
                },
                //TODO this is just the item ID to be used in URL - rework this to look better for updating both events and settings
                { d: { hiddenEventId: newSharepointListItem.Id } }
            );
        }
        else {
            //post new item to the list
            apiPostListContent(
                "Resources",
                newSharepointListItem,
                function (d) {
                    showSettingSavedOKIcon();
                    removeAllSettingSavedIcons();
                    //assign returned Id from server to the item in the list on screen
                    $(".cc-res-row:last-child .cc-setting-id").val(d.d.Id);
                    //refresh resources on screen immediately
                    C.fullCalendar('refetchResources');
                },
            function (d) {
                showSettingErrorSavingIcon();
                removeAllSettingSavedIcons();
            });
        }
    }
    else if (clickTargetParent.hasClass("cc-colors-row")) {
        
        var newColorToPost = {
            id: clickTargetParent.children().filter(".cc-setting-id").val(),
            name: clickTargetParent.children().filter(".cc-colors-type").val().trim(),
            color: clickTargetParent.children().filter(".cc-colors-colorcode").val().trim()
        };

        //validate data 
        if (newColorToPost.name === "" && newColorToPost.color === "") { return; } //do not post if user filled in nothing

        // create new SP object
        var newSharepointListItem = {
            "__metadata": { "type": getListItemType("Event_Types") },
            "EventTypeName": newColorToPost.name,
            "ColorCode": newColorToPost.color,
            "InstanceIdentifier": instanceId
        };

        //copy id if item already exists
        if (newColorToPost.id != "") {
            newSharepointListItem.Id = newColorToPost.id;
        }

        //if no id, post new item; if id is not empty, then just update existing item
        if (newColorToPost.id != "") {
            apiUpdateListContent(
                "Event_Types",
                newSharepointListItem,
                function (d) {
                    showSettingSavedOKIcon();
                    removeAllSettingSavedIcons();
                    C.fullCalendar('refetchEvents');
                },
                function (d) {
                    showSettingErrorSavingIcon();
                    removeAllSettingSavedIcons();
                },
                //TODO this is just the item ID to be used in URL - rework this to look better for updating both events and settings
                { d: { hiddenEventId: newSharepointListItem.Id } }
            );
        }
        else {
            //post new item to the list
            apiPostListContent(
                "Event_Types",
                newSharepointListItem,
                function (d) {
                    showSettingSavedOKIcon();
                    removeAllSettingSavedIcons();
                    //assign returned Id from server to the item in the list on screen
                    $(".cc-colors-row:last-child .cc-setting-id").val(d.d.Id);
                    C.fullCalendar('refetchEvents');
                },
            function (d) {
                showSettingErrorSavingIcon();
                removeAllSettingSavedIcons();
            });
        }
    }
}

/** deletes a single resource or color/travel type from the settings window */
function deleteResourceOrColor(itemToDelete) {
    //decide whether we're in Colors or Resources settings
    if (itemToDelete.settingType === "res") {
        deleteResource(itemToDelete.id);
    }
    else if (itemToDelete.settingType === "colors") {
        deleteColor(itemToDelete.id);
    }
}

function deleteResource(resourceId) {
    apiDeleteListContent(
        "Resources",
        resourceId, //TODO will it match the actual item id on server? even if item deleted/recreated? 
        function (dataFromServer, deletedItemId) {
            var rowToRemove = $(".cc-res-row input.cc-setting-id[value=" + deletedItemId + "]").parent();
            rowToRemove.slideUp(100);
            setTimeout(function () {
                rowToRemove.remove();
            }, 100);
            $(".cssSpinner").hide();
            C.fullCalendar('refetchResources');
        },
        function (dataFromServer) {
            //TODO show error
            $(".cssSpinner").hide();
        });
}

function deleteColor(travelTypeID) {
    apiDeleteListContent(
        "Event_Types",
        travelTypeID, //TODO will it match the actual item id on server? even if item deleted/recreated? 
        function (dataFromServer, deletedItemId) {
            var rowToRemove = $(".cc-colors-row input.cc-setting-id[value=" + deletedItemId + "]").parent();
            rowToRemove.slideUp(100);
            setTimeout(function () {
                rowToRemove.remove();
            }, 100);
            $(".cssSpinner").hide();
            C.fullCalendar('refetchEvents');
        },
        function (dataFromServer) {
            //TODO show error
            $(".cssSpinner").hide();
        });
}

/** removes all icons from the settings window after inline editing/saving */
function removeAllSettingSavedIcons() {
    setTimeout(function () {        
        $(".cc-setting-save-status-icon").remove();
    }, 900);
}

/** shows OK icon after saving a setting after inline editing */
function showSettingSavedOKIcon() {
    $(".cc-setting-save-status-icon").html("").addClass("cc-setting-saved").show().fadeOut(900);
}

/** shows error icon after saving a setting after inline editing */
function showSettingErrorSavingIcon() {
    $(".cc-setting-save-status-icon").html("").addClass("cc-setting-error-saving").show().fadeOut(900);
}

function attachButtonIcons() {
    $(".fc-leftCreateButton-button").prepend('<i class="fa fa-plus" aria-hidden="true"></i>');
    $(".fc-settingsButton-button").prepend('<i class="fa fa-cog" aria-hidden="true"></i>');
}

/** restores expand/collapse state of groups */ 
function restoreCollapsedResourceGroups() {
    /*
    calView = $('#calendar').fullCalendar('getView');
    function collapseLocations() {
        for (x = 0; x < calView.rowHierarchy.children.length; x++) {
            thisRow = calView.rowHierarchy.children[x];
            if (thisRow.groupValue != 'Resource A') {
                thisRow.collapse();
            }
        };
    */
}

/* license verification code */
window.SF = window.SF || {};
window.SF.LicenseConstructor = function ($) {
    var licenseCollection;
    var response;
    var licenseSettings;


    this.Check = function (s) {
        licenseSettings = s;
        var token;
        if ($.cookie) {
            token = $.cookie(licenseSettings.productId);
        }
        if (token) checkToken(token);
        else {
            licenseCollection = SP.Utilities.Utility.getAppLicenseInformation(licenseSettings.context, licenseSettings.productId);
            licenseSettings.context.executeQueryAsync(onRetrieveLicenseFromSPSuccess, onRetrieveLicenseFromSPFailure);
        }
    };


    function onRetrieveLicenseFromSPSuccess() {
        var topLicense;
        var encodedTopLicense;


        if (licenseCollection.get_count() > 0) {
            topLicense = licenseCollection.get_item(0).get_rawXMLLicenseToken();
            encodedTopLicense = encodeURIComponent(topLicense);
        } else {
            Redirect(licenseSettings.licenseUrl);
        }


        var request = new SP.WebRequestInfo();
        request.set_url("https://verificationservice.officeapps.live.com/ova/verificationagent.svc/rest/verify?token=" + encodedTopLicense);
        request.set_method("GET");
        response = SP.WebProxy.invoke(licenseSettings.context, request);
        licenseSettings.context.executeQueryAsync(onVerificationCallSuccess, onVerificationCallFailure);
    }


    function onRetrieveLicenseFromSPFailure(sender, args) {
        Redirect(licenseSettings.licenseUrl);
    }


    function onVerificationCallSuccess() {
        var xmltoken = response.get_body();
        if ($.cookie) {
            $.cookie(licenseSettings.productId, xmltoken, {
                expires: 180
            });
        }
        checkToken(xmltoken);
    }


    function checkToken(xmltoken) {
        var token = $.xml2json(xmltoken);
        switch (token.VerifyEntitlementTokenResponse.EntitlementType.toLowerCase()) {

            case "free":
                break;
            case "paid":
                break;
            case "trial":
                if (token.VerifyEntitlementTokenResponse.IsEntitlementExpired.toLowerCase() === "true") Redirect(licenseSettings.expiredUrl); // Trial app has expired!
                break;
        }
    }


    function onVerificationCallFailure() {
        Redirect(licenseSettings.licenseUrl);
    }


    function Redirect(url) {
        window.location.href = url;
    }
    return this;
};

window.SF.License = new SF.LicenseConstructor(jQuery);

var licenseSettings = {
    context: SP.ClientContext.get_current(),
    licenseUrl: "",//"https://celgosystems-75d475b064d13e.sharepoint.com/sites/Development/CelgoCalendar/Reservation/",
    expiredUrl: "TrialExpired.aspx",
    productId: "{a70c102d-daa6-4f8c-8547-4cd05ef0e4b2}"
};
SF.License.Check(licenseSettings);

/* end license code */

/** attaches a tooltip to color code input in settings */
function attachColorCodeToolTip(elementsToAttachTo) {
    addToolTip(elementsToAttachTo, {
        title: "Color for this type of event",
        text: "Events in calendar will have this color.<br><br>" +
                "Use either an HTML color code or an HTML color name.<br><br>" +
                "<strong>Examples:</strong><br>" +
                "<ul><li>DarkRed</li><li>DeepSkyBlue</li><li>Green</li><li>Teal</li><li>SteelBlue</li><li>YellowGreen</li><li>#53777A</li><li>#79BD9A</li><li>#D95B43</li></ul>"
    },
    null,
    "mouseleave");
}

/** attaches a tooltip to sorting order field in settings */
function attachSortingOrderToolTip(elementsToAttachTo) {
    addToolTip(elementsToAttachTo, {
        title: "Sorting order in calendar",
        text: "Determines the sorting order of this resource in the calendar. The higher the number, the higher it will appear in the list. " +
                "<br><br>For example, use 1 to place resources on top of the resources list, and 99 to plce them on bottom."
    },
    null,
    "mouseleave");
}

/** closes modal dialog if user clicked/tapped outside the modal window */
function attachClickOutsideToClose(screenName) {
    //Timeout because click is triggered on touchscreens only 300-350 ms after touchend; if not using timeout, window will close immediately
    setTimeout(function () {
        $(".modalPopup").on('click', function (event) {
            if (!$(event.target).closest('.modalPopup > div > div > div').length &&
                /* MS people picker is buggy */
                !$(event.target).closest(".ms-core-menu-link, .sp-peoplepicker-resolveList, .sp-peoplepicker-delImage").length) {
                    hideScreen(screenName);
            }
        });
    },400);
}

/** checks for browser touch events support, currently only webkit */
function checkBrowserTouchSupport() {
    return ('ontouchstart' in window);
}