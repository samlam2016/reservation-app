﻿function attachFilterScreenHandlers() {
    $("#filterScreen button:nth-of-type(2)").click(function () {
        hideScreen("filter");
    });

    $("#filterScreen button:nth-of-type(1)").click(function () {
        spinnerShow();
        saveUserEnteredFilters();
        //without timeout, render gets blocked and spinner won't show until filtering completes
        setTimeout(function () {
            C.fullCalendar('refetchResources');
            hideScreen("filter");
        }, 50);
    });
}

function showFilterScreen() {

    // set checkboxes according to the saved state
    var f = getFilter();
    if (f[0] === "all") {
        $(".gradeFilterCheckboxes").prop("checked", true);
        $("#toggleAll").prop("checked", true);
    }
    else {
        $(".gradeFilterCheckboxes").each(function () {
            var checkboxLabelText = $(this).next().html();
            if (($.grep(f, function (arrayItem) { return checkboxLabelText === arrayItem; })).length > 0) {
                $(this).prop("checked", true);
            }
        });
    }
    // set Select All checkbox

    $("#filterScreen").fadeIn();
}

/* Get resource IDs from checked checkboxes in the Filter screen and save them to localStorage */
function saveUserEnteredFilters() {
    var appliedFilters = $(".gradeFilterCheckboxes:checked");
    var filtersToSave = [];
    for (var i = 0; i < appliedFilters.length; i++) {
        filtersToSave.push(appliedFilters[i].value);
    }
    setFilter(filtersToSave);
}

/* accepts an array of strings that will be the new filter settings (will persist) */
function setFilter(newFilterArray) {
    // if all checkboxes selected, set filter to "all" instead of individual values, otherwise newly added categories won't show
    if (newFilterArray.length === ($(".gradeFilterCheckboxes")).length) {
        newFilterArray = ["all"];
    }
    window.localStorage["fcGradeFilterStorage"] = JSON.stringify(newFilterArray);
}

/* returns an array of filtered-in employee grades */
function getFilter() {
    //if first time load
    if (window.localStorage["fcGradeFilterStorage"] === undefined || window.localStorage["fcGradeFilterStorage"] === "undefined") {
        window.localStorage["fcGradeFilterStorage"] = '["all"]';
    }

    return JSON.parse(window.localStorage["fcGradeFilterStorage"]);
}

function populateFilterCheckboxes() {
    //populate checkboxes in Filter screen TODO redo for new data model


    /*
    $.ajax({
        url: globalSharepointUrl + "_api/web/lists/getbytitle('EMPLOYEEGRADESLISTNAME')/items?$orderby=Order_in_Calendar,Title",
        type: 'GET',
        headers: {
            "accept": "application/json; odata=verbose"
        },
        success: function (egd) {
            egd = egd.d.results;
            globalEmployeeGrades = egd;

            var checkboxesHtml = '';
            for (var i = 0; i < egd.length; i++) {
                checkboxesHtml +=
                    '<div>' +
                    '<input type="checkbox" class="gradeFilterCheckboxes" id="filter' + egd[i].Id + '" ' +
                    'value="' + egd[i].Title + '">' +
                    '<label for="filter' + egd[i].Id + '">' +
                    egd[i].Title + '</label>' +
                    '</div>';
            }
            $("#filterCheckboxesContainer").html(checkboxesHtml);

            // attach better UX for filter checkboxes: switch on / switch off "Select All" depending on the state of user-selected filters
            $(".gradeFilterCheckboxes").change(function () {
                if (($(".gradeFilterCheckboxes")).length === ($(".gradeFilterCheckboxes:checked")).length) {
                    $("#toggleAll").prop("checked", true);
                }
                else {
                    $("#toggleAll").prop("checked", false);
                }
            });
        },
        error: function (egd) {
            console.log("Error fetching employee grades from server.");
            console.log(egd);
        }
    }); //populate checkboxes

    */
}

/* checks whether OK to render event, i.e. whether it was filtered in/out */
function isEventFilteredIn() {
    var f = getFilter();
    if (f[0] === "all") {
        return true;
    } else {
        var filterActive = $.grep(f, function (e) {
            return e === eventGrade;
        });

        if (filterActive.length === 0) {
            //don't show event if didn't match filter
            return false;
        }
        else {
            return true;//ok to render event
        }
    }
}