﻿/** shows error message when switch statement has a wrong calendar type specified */
function typeSwitchError() {
    console.log("Wrong calendar type specified in code.");
}

/** gets text for top left cell in scheduler view */
function getResourceLabelText() { 
    switch (getCalType()) {
        case "meetings":
            return " ";
            //break;
        case "roombooking":
            return " ";
            //break;
        case "carbooking":
            return "Vehicles";
            //break;
        default:
            typeSwitchError();
    }
}

/** gets text for the Create New button */
function getCreateButtonText() {
    switch (getCalType()) {
        case "meetings":
            return " New";
            //break;
        case "roombooking":
            return " New";
            //break;
        case "carbooking":
            return " New";
            //break;
        default:
            typeSwitchError();
    }    
}

/** shows 'create new' screen depending on the calendar type in settings */
function showNewScreenDependingOnType(date) {
    switch (getCalType()) {
        case "meetings":
            showCreateMeetingScreen(date);
            break;
        case "roombooking":
            showCreateRoomBookingScreen(date);
            break;
        case "carbooking":
            showCarBookingScreen(date);
            break;
        default:
            typeSwitchError();
    }
}

/** attaches actions to Save/Cancel buttons in modal dialogs */
function attachSaveCancelButtons() {
    switch (getCalType()) {
        case "meetings":
            /* cancel buttons */
            $("#createScreen button.cc-button-cancel").click(function () {
                hideScreen("create");
            });

            $("#editScreen button.cc-button-cancel").click(function () {
                hideScreen("edit");
            });

            /* save/apply buttons */
            $("#createScreen button.cc-button-ok").click(function () {
                addNewEvent();
            });

            $("#editScreen button.cc-button-ok").click(function () {
                saveEditedEvent();
            });
            break;
        case "roombooking":
            $("#bookRoomScreen .cc-button-cancel").click(function () {
                hideScreen("bookRoom");
            });
            $("#bookRoomScreen .cc-button-ok").click(function () {
                saveRoomBooking();
            });
            $("#bookRoomScreen .cc-button-danger").click(function () {
                deleteEvent($("#bookRoomScreen .hiddenEventId").val());
            });
            break;
        case "carbooking":
            $("#bookCarScreen .cc-button-cancel").click(function () {
                hideScreen("bookCar");
            });
            break;
        default:
            typeSwitchError();
    }

    /* settings screen is the same for all views */
    $("#cc-settings-screen .cc-button-ok").click(function () {
        hideScreen("settings");
    });

    /* same for Filter screen */
    attachFilterScreenHandlers();
}

/** sets date/time picker options and attaches date/time pickers to inputs depending on calendar type */ 
function attachDateTimePickers() {
    var dateTimePickerSettings = {
        format:         'Y-m-d H:i',
        dayOfWeekStart: 1, // Monday
        allowedTimes: [ '00:00', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00',
                        '12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00',
                        '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '23:59']
    }; 

    switch (getCalType()) {
        case "meetings":
            attachIndividualDateTimePicker("#createScreen .formStartDate", dateTimePickerSettings);
            attachIndividualDateTimePicker("#createScreen .formEndDate", dateTimePickerSettings);
            attachIndividualDateTimePicker("#editScreen .formStartDate", dateTimePickerSettings);
            attachIndividualDateTimePicker("#editScreen .formEndDate", dateTimePickerSettings);
            break;
        case "roombooking":
            attachIndividualDateTimePicker("#bookRoomScreen .formStartDate", dateTimePickerSettings);
            attachIndividualDateTimePicker("#bookRoomScreen .formEndDate", dateTimePickerSettings);
            break;
        case "carbooking":

            break;
        default:
            typeSwitchError();
    }
}

/** attaches single customized date/time picker to a target input */
function attachIndividualDateTimePicker(targetSelector, settings) {
    $(targetSelector).datetimepicker({
        format: settings.format,
        dayOfWeekStart: settings.dayOfWeekStart,
        allowTimes: settings.allowedTimes,
        onChangeDateTime: function () {
            //remove red highlighting from the other datepicker after invalid input
            $(targetSelector).change();
        }
    });
}

function populateResourcesDropdown() {
    var outputHtml = '';

    var resources = originalResourcesSnapshot;

    if (resources === null) { return; }

    //sort by department first, then by resource name alphabetically
    resources = resources.sort(function (a, b) {
        if (a.department === b.department) {
            return a.title.localeCompare(b.title);
        } else {
            return a.department.localeCompare(b.department);
        }
    });


    switch (getCalType()) {
        case "meetings":
        case "roombooking":
        case "carbooking":
            for (var i = 0; i < resources.length; i++) {
                outputHtml +=
                    '<option value="' + resources[i].id + '">' +
                    //shorter display value for the meetings calendar type
                    //if name is not defined in settings, output "no name" instead of "null"
                    (
                        (getCalType() === "meetings") ?
                        (resources[i].title ? resources[i].title : "(no name " + resources[i].id + ")") :
                        resources[i].department + " – " + (resources[i].title ? resources[i].title : "(no name " + resources[i].id + ")")
                    )
                    +
                    "</option>";
            }
            //clear existing items before repopulating
            $("select.resourceSelect").html("<option value=''></option>");
            //now actually populate
            $("select.resourceSelect").append(outputHtml);

            //attach auto-complete
            //$("#createScreen .resourceSelect").editableSelect({ effects: 'slide' });
            //$("#editScreen .resourceSelect").editableSelect({ effects: 'slide' });
            //$("#bookRoomScreen .resourceSelect").editableSelect({ effects: 'slide' });
            //TODO problems getting value instead of full name
            break;
        default:
            typeSwitchError();
    }
}

/** gets data from the filled-out form and returns as a single object */
function getFormData() {
    switch (getCalType()) {
        case "meetings":
            return {
                hiddenEventId: $("#spEventId").val(),
                booker: (($("#editScreen .formLocation").val().trim() === "") ? $("#createScreen .formLocation").val().trim() : $("#editScreen .formLocation").val().trim()),
                type: (($("#editScreen .formTravelType").val().trim() === "") ? $("#createScreen .formTravelType").val().trim() : $("#editScreen .formTravelType").val().trim()),
                resourceId: null,//(($("#editScreen select.resourceSelect").val() == "") ? $("#createScreen select.resourceSelect").val() : $("#editScreen select.resourceSelect").val()),
                startDate: (($("#editScreen .formStartDate").val().trim() === "") ? $("#createScreen .formStartDate").val().trim() : $("#editScreen .formStartDate").val().trim()),
                endDate: (($("#editScreen .formEndDate").val().trim() === "") ? $("#createScreen .formEndDate").val().trim() : $("#editScreen .formEndDate").val().trim())
            };
            //break;
        case "roombooking":
            return {
                hiddenEventId:  $("#bookRoomScreen .hiddenEventId").val(),
                startDate:      $("#bookRoomScreen .formStartDate").val().trim(),
                endDate:        $("#bookRoomScreen .formEndDate").val().trim(),
                type:           $("#bookRoomScreen .formTravelType").val(),
                subject:        $("#bookRoomScreen .formSubject").val().trim(),
                attendees:      getPeoplePickerNames("PeopleEditor1", true),
                notes:          $("#bookRoomScreen .notesRequirements").val().trim(),
                booker:         $("#bookRoomScreen .currentUserName").val().trim(),
                resourceId:     $("#bookRoomScreen select.resourceSelect").val()
            };
            //break;
        case "carbooking":

            break;
        default:
            typeSwitchError();
    }
}

/** returns a Sharepoint List Item that can be posted to the server - for events only */
function getSharepointListItem(d, listName) {
    switch (getCalType()) {
        case "meetings":
            return {
                "__metadata": { "type": getListItemType(listName) },
                //"Attendees": d.attendees,
                "Requestor": d.booker,
                "Title": d.resourceId,
                "Type": d.type,
                "StartDate": moment(d.startDate).toISOString(),
                "EndDate": moment(d.endDate).toISOString(),
                "IsFullDay": false,//TODO set to actual value
                //"Comment": d.notes,
                //"HandledBy": null,
                "InstanceIdentifier": instanceId
            };
            //break;
        case "roombooking":
            return {
                "__metadata": { "type": getListItemType(listName) },
                "Attendees": d.attendees,
                "Requestor": d.booker,
                "Title": d.resourceId,
                "Type": d.type,
                "StartDate": moment(d.startDate).toISOString(),
                "EndDate": moment(d.endDate).toISOString(),
                "IsFullDay": false,//TODO set to actual value
                "Comment": d.notes,
                "HandledBy": null,
                "InstanceIdentifier": instanceId
            };
            //break;
        case "carbooking":
            //TODO
            break;
        default:
            typeSwitchError();
    }
}

/** creates renders a newly created fullCalendar event and returns the created Event object */
function createAndRenderEvent(d) {
    switch (getCalType()) {
        case "meetings":
            return C.fullCalendar('renderEvent', {
                start: moment(d.startDate),
                end: moment(d.endDate),
                color: getColorCodeByTravelTypeId(d.type),
                travelTypeId: d.type,
                title: d.booker, //d.subject,
                //attendees: d.attendees,
                //notes: d.notes,
                booker: d.booker,
                resourceId: d.resource
            }, false); //false = not sticky
            //break;
        case "roombooking":
            return C.fullCalendar('renderEvent', {
                start: moment(d.startDate),
                end: moment(d.endDate),
                color: getColorCodeByTravelTypeId(d.type),
                travelTypeId: d.type,
                title: d.booker, //d.subject,
                attendees: d.attendees,
                notes: d.notes,
                booker: d.booker,
                resourceId: d.resource
            }, false); //false = not sticky
            //break;
        case "carbooking":
            //TODO
            break;
        default:
            typeSwitchError();
    }
}

/** called when new event is successfully saved on the server */
function saveNewEventSuccess(dataFromServer, nextFunctionArgs) {
    switch (getCalType()) {
        case "meetings":
            //after successful save to server, create new event on screen 
            var newFullCalendarEvent = createAndRenderEvent(nextFunctionArgs.d);

            // assign real database-originated ID to the event on screen which currently has no ID, otherwise won't be able to edit it until refresh
            newFullCalendarEvent[0].id = dataFromServer.d.Id;
            newFullCalendarEvent[0]._id = dataFromServer.d.Id;
            C.fullCalendar('updateEvent', newFullCalendarEvent[0]);
            C.fullCalendar('refetchEvents');//TODO hack, shouldn't be this way, too slow

            hideScreen("create");
            hideScreen("edit");

            break;
        case "roombooking":
            //after successful save to server, create new event on screen 
            var newFullCalendarEvent = createAndRenderEvent(nextFunctionArgs.d);

            // assign real database-originated ID to the event on screen which currently has no ID, otherwise won't be able to edit it until refresh
            newFullCalendarEvent[0].id = dataFromServer.d.Id;
            newFullCalendarEvent[0]._id = dataFromServer.d.Id;
            C.fullCalendar('updateEvent', newFullCalendarEvent[0]);
            C.fullCalendar('refetchEvents');//TODO hack, shouldn't be this way, too slow

            hideScreen("bookRoom");
            break;
        case "carbooking":
            //TODO
            break;
        default:
            typeSwitchError();
    }
}

/** called if ajax call to server fails */
function saveNewEventError(d) {
    console.log("Error submitting new event to server");
    console.log(d);
    showFormError("Couldn't save. Are you connected to the Internet?");
    $(".cssSpinner").hide();
}

/** shows edit screen for the event */
function showEditScreen(event) {
    switch (getCalType()) {
        case "meetings":
            //set form fields
            $("#editScreen .formLocation").val(event.title);
            $("#editScreen .formTravelType option[value='" + event.travelTypeId + "']").prop("selected", true);
            $("#editScreen .formTravelType").change();
            $("#editScreen select.resourceSelect option[value='" + event.resourceId + "']").prop("selected", true);
            $("#editScreen .formStartDate").val(event.start.format('YYYY-MM-DD HH:mm'));
            $("#editScreen .formEndDate").val(event.end.format('YYYY-MM-DD HH:mm'));
            $("#editScreen #spEventId").val(event.id);
            $("#editScreen").fadeIn();
            attachClickOutsideToClose("edit");
            break;
        case "roombooking":
            attachRoomBookingScreenHandlers();
            $("#bookRoomScreen .cc-button-danger").show();
            //set form fields
            $("#bookRoomScreen .hiddenEventId").val(event.id);
            $("#bookRoomScreen .formStartDate").val(event.start.format('YYYY-MM-DD HH:mm'));
            $("#bookRoomScreen .formEndDate").val(event.end.format('YYYY-MM-DD HH:mm'));
            $("#bookRoomScreen .formTravelType option[value='" + event.travelTypeId + "']").prop("selected", true);
            $("#bookRoomScreen .formTravelType").change();
            $("#bookRoomScreen .notesRequirements").val(event.notes);
            $("#bookRoomScreen .currentUserName").val(event.booker);
            $("#bookRoomScreen select.resourceSelect option[value='" + event.resourceId + "']").prop("selected", true);
            setPeoplePicker("PeopleEditor1", event.attendees);
            $("#bookRoomScreen button.cc-button-ok").html("Save");
            attachClickOutsideToClose("bookRoom");
            //show screen
            $("#bookRoomScreen").fadeIn();
            break;
        case "carbooking":
            //TODO
            attachClickOutsideToClose("bookCar");
            break;
        default:
            typeSwitchError();
    }
}

/** clears all form fields, typically when the form is closed, so that next time all fields can be set anew */
function clearAllFormFields(formName) {
    switch (formName) {
        case "bookRoom":
            $("#bookRoomScreen .hiddenEventId").val("");
            $("#bookRoomScreen .formStartDate").val("");
            $("#bookRoomScreen .formEndDate").val("");
            $("#bookRoomScreen select.formTravelType option:selected").removeAttr("selected");
            $("#bookRoomScreen .formSubject").val("");
            $("#bookRoomScreen .notesRequirements").val("");
            $("#bookRoomScreen .currentUserName").val("");
            $("#bookRoomScreen select.resourceSelect option:selected").removeAttr("selected");
            $("#bookRoomScreen .resourceDescription").html("");
            clearPeoplePicker("PeopleEditor1");
            break;
        case "edit":
            $("#editScreen #spEventId").val("");
            $("#editScreen .formLocation").val("");
            $("#editScreen .formTravelType option:selected").removeAttr("selected");
            $("#editScreen select.resourceSelect option:selected").removeAttr("selected");
            $("#editScreen .formStartDate").val("");
            $("#editScreen .formEndDate").prop("disabled", false).val("");
            $("#editScreen .fullDayCheckbox").prop("checked", false);
            break;
        case "create":
            $("#createScreen .formLocation").val("");
            $("#createScreen .formTravelType option:selected").removeAttr("selected");
            $("#createScreen select.resourceSelect option:selected").removeAttr("selected");
            $("#createScreen .formStartDate").val("");
            $("#createScreen .formEndDate").val("");
            $("#createScreen .formEndDate").prop("disabled", false).val("");
            $("#createScreen .fullDayCheckbox").prop("checked", false);
            break;
        case "firstRun":
        case "settings":
            break;
        default:
            typeSwitchError();
    }
    $(".cc-edit-form-color-sampler").css("background-color", "transparent");
}

/** returns custom views and their options based on the calendar type */
function getFullCalendarViews() {
    switch (getCalType()) {
        case "meetings":
            return {
                agendaDay: {
                    scrollTime: '08:00',
                    resources: false
                }
            };
            //break;
        case "roombooking":
            return {
                timelineDay: {
                    slotDuration: '01:00',
                    scrollTime: '08:00'
                },
                sevenDays: {
                    type: 'timeline',
                    duration: { days: 7 },
                    buttonText: '7',
                    slotDuration: '12:00',
                    slotLabelFormat: ['ddd D'] //Mon 7
                },
                fourteenDays: {
                    type: 'timeline',
                    duration: { days: 14 },
                    buttonText: '14',
                    slotDuration: '12:00',
                    slotLabelFormat: ['ddd D']
                }
            };
            //break;
        case "carbooking":
            return {
                timelineDay: {
                    slotDuration: '01:00'
                },
                sevenDays: {
                    type: 'timeline',
                    duration: { days: 7 },
                    buttonText: '7',
                    slotDuration: '12:00',
                    slotLabelFormat: ['ddd D'] //Mon 7
                },
                fourteenDays: {
                    type: 'timeline',
                    duration: { days: 14 },
                    buttonText: '14',
                    slotDuration: '12:00',
                    slotLabelFormat: ['ddd D']
                }
            };
            //break;
        default:
            typeSwitchError();
    }
}

/** returns custom text for fullcalendar's top buttons that override default text */
function getFullCalendarButtonText() {
    switch (getCalType()) {
        case "meetings":
            return { month: "M", basicDay: "1", basicWeek: "7", agendaDay: "1" };
            //break;
        case "roombooking":
            return { month: "M", timelineDay: "1" };
            //break;
        case "carbooking":
            return { month: "M", timelineDay: "1" };
            //break;
        default:
            typeSwitchError();
    }
}

function getFullCalendarViewButtons() {
    switch (getCalType()) {
        case "meetings":
            return 'agendaDay,basicWeek,month';
            //break;
        case "roombooking":
            return 'timelineDay,sevenDays,month';
            //break;
        case "carbooking":
            return 'timelineDay,sevenDays,month';
            //break;
        default:
            typeSwitchError();
    }
}

function getFullCalendarDefaultView() {
    switch (getCalType()) {
        case "meetings":
            return 'month';
            break;
        case "roombooking":
            return 'sevenDays';
            break;
        case "carbooking":
            return 'timelineDay';
            break;
        default:
            typeSwitchError();
    }
}

/** saves the event after it's been dragged and dropped, or reverts to where it was before if the ajax to server fails */
function eventDropOrResizeHandler(event, delta, revertFunc, jsEvent, ui, view) {
    switch (getCalType()) {
        case "meetings":
        case "roombooking":
            var d = {
                attendees: event.attendees,
                booker: event.booker,
                endDate: event.end.format("YYYY-MM-DD HH:mm"),
                hiddenEventId: event.id,
                notes: event.notes,
                resourceId: event.resourceId,
                startDate: event.start.format("YYYY-MM-DD HH:mm"),
                subject: event.subject,
                type: event.travelTypeId
            };

            //create new sharepoint list item before saving it to server
            var newSharepointListItem = getSharepointListItem(d, "Events");

            //otherwise there will be an error when trying to save an existing event with a deleted event type, or simply an event without a selected event type
            if (newSharepointListItem.Type == "") { newSharepointListItem.Type = 0; }

            //update existing item on server, revert to original state if error
            apiUpdateListContent("Events", newSharepointListItem, function () {/* nothing */ }, revertFunc, { d: d });

            break;
        case "carbooking":
            revertFunc();
            break;
        default:
            revertFunc();
            typeSwitchError();
    }
}

/** verify that form inputs are correct and display error messages; return true if all ok */
function verifyFormInputIsOK(d) {
    switch (getCalType()) {
        case "meetings":
            if (//verifyNotEmpty(d.resourceId, "Please select a resource from the dropdown list.", "select.resourceSelect") && //no resources for this calendar type
                verifyStartEndDates(d.startDate, d.endDate)) {
                return true;
            }
            return false;
            //break;
        case "roombooking":
            if (verifyNotEmpty(d.resourceId, "Please select a resource from the dropdown list.", "#bookRoomScreen select.resourceSelect") &&
                verifyStartEndDates(d.startDate, d.endDate)) {
                return true;
            }
            return false;
            //break;
        case "carbooking":

            break;
        default:
            typeSwitchError();
    }
}

/** verifies that a form input from user is not empty, shows message and highlights field with error */
function verifyNotEmpty(data, msg, selectorToHighlight) {
    if (data.trim() === "") {
        showFormError(msg);
        makeRed(selectorToHighlight);
        return false;
    }
    return true;
}

/** verify that start and end dates entered by user are not empty, start not later than end, and total duration not longer than a limit */
function verifyStartEndDates(startDate, endDate) {
    // check for empty dates
    if (startDate.trim() === "" || endDate.trim() === "") {
        showFormError("Please enter both start and end date.");
        if (startDate.trim() === "") {
            makeRed(".formStartDate");
        }
        if (endDate.trim() === "") {
            makeRed(".formEndDate");
        }
        return false;
    }

    //convert to unix timestamp for comparison; new Date doesn't work reliably in IE
    startDate = parseInt(moment(startDate).format("x"));
    endDate = parseInt(moment(endDate).format("x"));

    /* will crash new event creation otherwise */
    if (startDate >= endDate) {
        showFormError("Start date has to be earlier than end date.");
        makeRed(".formStartDate");
        makeRed(".formEndDate");
        return false;
    }

    /* event limited to 45 days - this is because of filters in the event loading URL. Longer events will not be loaded. */
    if ((endDate - startDate) > 3888000001) { //45 days
        showFormError("Events should be less than 45 days.");
        makeRed(".formEndDate");
        return false;
    }

    return true;
}

/** returns eventOverlap parameter for fullcalendar depending on the calendar type */
function getEventOverlap() {
    switch (getCalType()) {
        case "meetings":
            return true;
            //break;
        case "roombooking":
            return false;
            //break;
        case "carbooking":
            return false;
            //break;
        default:
            typeSwitchError();
    }
}

/** returns event tooltip text depending on the calendar type */
function getEventToolTip(event, element) {
    var nonNullEventTitle = ((event.title === null) ? "" : event.title + ", ");
    switch (getCalType()) {
        case "meetings":
            return nonNullEventTitle + getTooltipDateText(event.start, event.end);
        case "roombooking":
            return nonNullEventTitle + getTooltipDateText(event.start, event.end) + "<br><br>" + getResourceNameById(event.resourceId) + ((event.notes) ? "<br><br>" + event.notes : "");
        case "carbooking":
            return nonNullEventTitle + getTooltipDateText(event.start, event.end) + "<br><br>" + getResourceNameById(event.resourceId) + ((event.notes) ? "<br><br>" + event.notes : "");
        default:
            typeSwitchError();
            return "";
    }
}

/** returns the text that is displayed on events in calendar views */
function getEventDisplayText(event, element) {
    switch (getCalType()) {
        case "meetings":
            if (event.title === null || event.title === "") { //it can be &nbsp; if empty
                return event.start.format('D MMM HH:mm') + " – " + event.end.format('D MMM HH:mm');
            }
            else {
                return event.title;
            }
            //break;
        case "roombooking":
            if (event.notes !== undefined && event.notes !== null && event.notes.trim() !== "") {
                return event.notes;
            }
            else {
                return event.title;
            }
            //break;
        case "carbooking":
            
            break;
        default:
            typeSwitchError();
            return "";
    }
}

/* template 

    switch (getCalType()) {
        case "meetings":

            break;
        case "roombooking":

            break;
        case "carbooking":
            
            break;
        default:
            typeSwitchError();
    }

*/